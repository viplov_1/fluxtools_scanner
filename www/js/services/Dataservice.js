'use strict';

angular.module('starter')
    .service('Dataservice', function Dataservice($q, $cordovaSQLite) {
        return {
            insertIntoTask: function (id, name, description, syncStatus, uri, type, map, itemCount) {
                var deferred = $q.defer();
                console.log(map);
                console.log(type);
                if (!type) {
                    type = "";
                }
                var query = "INSERT INTO tasks (id, name, description, syncStatus, uri, type, map, itemCount) VALUES (?,?,?,?,?,?,?,?)";
                $cordovaSQLite.execute(db, query, [id, name, description, syncStatus, uri, type, map, itemCount]).then(function (res) {
                    console.log(res);
                    deferred.resolve();
                }, function (err) {
                    console.error(err);
                });
                return deferred.promise;
            },
            selectFromTask: function () {
                var deferred = $q.defer();
                var localTasks = [];
                var query = "SELECT id, name,description,syncStatus,itemCount,uri,map, type FROM tasks";
                $cordovaSQLite.execute(db, query).then(function (res) {
                    console.log(res);
                    if (res.rows.length > 0) {
                        deferred.resolve(res);
                    } else {
                        deferred.resolve([]);
                    }
                }, function (err) {
                    console.error(err);
                    deferred.reject(err);
                });
                return deferred.promise;
            },
            getPlan: function (map) {
                console.log("Trying to get plan for: " + map);
                var deferred = $q.defer();
                var query = "SELECT plan FROM floorPlan where fpId='" + map + "'";
                $cordovaSQLite.execute(db, query).then(function (res) {
                    console.log(res);
                    if (res.rows.length > 0) {
                        deferred.resolve(res.rows.item(0).plan);
                    } else {
                        deferred.reject(map);
                    }
                }, function (err) {
                    console.error(err);
                    deferred.reject(map);
                });
                return deferred.promise;
            },
            insertPlan: function (id, plan) {
                var deferred = $q.defer();
                var query = "INSERT INTO floorPlan (fpId, plan) VALUES (?,?)";
                $cordovaSQLite.execute(db, query, [id, plan]).then(function (res) {
                    console.log(res);
                    deferred.resolve(res);
                }, function (err) {
                    console.error(err);
                    deferred.reject("Plan updation failed");
                });
            },
            selectItemCount: function (taskId) {
                var deferred = $q.defer();
                var query = "Select * from tags where taskId='" + taskId + "'";
                $cordovaSQLite.execute(db, query).then(function (res) {
                    var count = res.rows.length;
                    console.log("TaskId: " + taskId);
                    console.log("Count: " + count);
                    deferred.resolve(count);
                    //updateItemCount(taskId, count);
                }, function (err) {
                    console.error(err);
                    deferred.resolve(0);
                });
                return deferred.promise;
            },
            getTags: function (taskId) {
                var deferred = $q.defer();
                var query = "Select tagId FROM tags where taskId='" + taskId + "'";
                $cordovaSQLite.execute(db, query).then(function (res) {
                    var currentTags = [];
                    if (res.rows.length > 0) {
                        console.log(res.rows);
                        for (var j = 0; j < res.rows.length; j++) {
                            currentTags.push(res.rows.item(j).tagId);
                        }
                    }
                    deferred.resolve(currentTags);
                }, function (err) {
                    console.error(err);
                    deferred.reject(err);
                });
                return deferred.promise;
            },
            updateSyncStatus(taskId, status) {
                var deferred = $q.defer();
                var syncStatus = "";
                if (!status) {
                    syncStatus = "Sync failed";
                }
                console.log(syncStatus);
                console.log(taskId);
                var query = "Update tasks set syncStatus='" + syncStatus + "' where id='" + taskId + "'";
                $cordovaSQLite.execute(db, query).then(function (res) {
                    console.log(res);
                    deferred.resolve();
                }, function (err) {
                    console.error(err);
                    deferred.reject();
                });
                return deferred.promise;
            },
            updateSyncReason: function (tagId, status, syncReason) {
                var deferred = $q.defer();
                var syncStatus = "";
                if (!status) {
                    syncStatus = "Sync failed";
                }
                if (!syncReason || syncReason === "") {
                    syncReason = "Connection failed";
                }
                if (status) {
                    syncReason = "";
                }
                var query = "Update devices set syncStatus='" + syncStatus + "', syncReason='" + syncReason + "' where tagId='" + tagId + "'";
                $cordovaSQLite.execute(db, query).then(function (res) {
                    console.log("Tags updated");
                    deferred.resolve();
                }, function (err) {
                    console.error(err);
                    deferred.reject();
                });
                return deferred.promise;
            },
            deleteAttachments: function (tagId) {
                var deferred = $q.defer();
                var query = "";
                if (tagId !== "") {
                    query = "Delete FROM attachments where tagId='" + tagId + "'";
                }
                else {
                    query = "Delete FROM attachments";
                }
                $cordovaSQLite.execute(db, query).then(function (res) {
                    console.log('Attachment deleted');
                    deferred.resolve();
                }, function (err) {
                    console.error(err);
                    deferred.reject();
                });
                return deferred.promise;
            },
            deleteDevices: function (tagId) {
                var deferred = $q.defer();
                var query = "";
                if (tagId !== "") {
                    query = "Delete FROM devices where tagId='" + tagId + "'";
                }
                else {
                    query = "Delete FROM devices";
                }
                $cordovaSQLite.execute(db, query).then(function (res) {
                    console.log('Tags deleted');
                    deferred.resolve();
                }, function (err) {
                    console.error(err);
                    deferred.reject();
                });
                return deferred.promise;
            },
            deleteTask: function (taskId) {
                var deferred = $q.defer();
                var query = "";
                query = "Delete FROM tasks where id='" + taskId + "'";
                $cordovaSQLite.execute(db, query).then(function (res) {
                    console.log('Task deleted');
                    deferred.resolve();
                }, function (err) {
                    console.error(err);
                    deferred.reject();
                });
                return deferred.promise;
            },
            deleteTags(taskId, tagId) {
                var deferred = $q.defer();
                var query = "";
                if (taskId !== "" && tagId !== "") {
                    query = "Delete FROM tags where taskId='" + taskId + "' AND tagId='" + tagId + "'";
                }
                else {
                    query = "Delete FROM tags";
                }
                $cordovaSQLite.execute(db, query).then(function (res) {
                    console.log('Tags deleted');
                    deferred.resolve();
                }, function (err) {
                    console.error(err);
                    deferred.reject();
                });
                return deferred.promise;
            },
            getAttachments: function (tagId) {
                var deferred = $q.defer();
                var query = "Select * FROM attachments where tagId='" + tagId + "'";
                $cordovaSQLite.execute(db, query).then(function (res) {
                    var attachments = [];
                    if (res.rows.length > 0) {
                        for (var i = 0; i < res.rows.length; i++) {
                            var attachment = {
                                Comments: res.rows.item(i).comment,
                                Name: res.rows.item(i).fileName,
                                File: res.rows.item(i).attachment
                            };
                            attachments.push(attachment);
                        }
                    }
                    deferred.resolve(attachments)
                }, function (err) {
                    console.error(err);
                });
                return deferred.promise;
            },
            getDevices: function (tagId) {
                var deferred = $q.defer();
                var query = "Select * FROM devices where tagId='" + tagId + "'";
                $cordovaSQLite.execute(db, query).then(function (res) {
                    if (res.rows.length > 0) {
                        deferred.resolve(res.rows);
                    }
                    else {
                        deferred.resolve([]);
                    }
                }, function (err) {
                    console.error(err);
                });
                return deferred.promise;
            },
            clearTasks: function () {
                var deferred = $q.defer();
                var query = "Delete FROM tasks";
                $cordovaSQLite.execute(db, query).then(function (res) {
                    deferred.resolve(res.rows);
                }, function (err) {
                    console.error(err);
                });
                return deferred.promise;
            },
            dropAllTables: function () {
                $cordovaSQLite.execute(db, "DROP TABLE tasks");
                $cordovaSQLite.execute(db, "DROP TABLE tags");
                $cordovaSQLite.execute(db, "DROP TABLE devices");
                $cordovaSQLite.execute(db, "DROP TABLE attachments");
                $cordovaSQLite.execute(db, "DROP TABLE floorPlan");
                $cordovaSQLite.execute(db, "DROP TABLE replaceTable");
                $cordovaSQLite.execute(db, "CREATE TABLE IF NOT EXISTS tasks (id text primary key, name text,description text, syncStatus text, uri text, type text, map text, itemCount text)");
                $cordovaSQLite.execute(db, "CREATE TABLE IF NOT EXISTS tags (taskId text, tagId text)");
                $cordovaSQLite.execute(db, "CREATE TABLE IF NOT EXISTS devices (tagId text primary key, deviceId text, deviceName text, deviceDescription text, syncStatus text, syncReason text, coords text)");
                $cordovaSQLite.execute(db, "CREATE TABLE IF NOT EXISTS attachments (tagId text, fileName text, attachment text, comment text)");
                $cordovaSQLite.execute(db, "CREATE TABLE IF NOT EXISTS floorPlan (fpId text, plan text)");
                $cordovaSQLite.execute(db, "CREATE TABLE IF NOT EXISTS replaceTable (taskId text, oldTag text, oldDevice text, newTag text, newDevice text, comment text, syncReason text)");
            },
            insertLoginDetails(email, provisioning) {
                var deferred = $q.defer();
                var query = "INSERT INTO loginCache (email, provisioning) VALUES (?,?)";
                $cordovaSQLite.execute(db, query, [email, provisioning]).then(function (res) {
                    console.log("INSERT ID -> " + res.insertId);
                    deferred.resolve();
                }, function (err) {
                    console.error(err);
                });
                return deferred.promise;
            },
            getLoginDetails() {
                var deferred = $q.defer();
                var query = "Select * from loginCache";
                $cordovaSQLite.execute(db, query).then(function (res) {
                    console.log("INSERT ID -> " + res.insertId);
                    if (res.rows.length > 0) {
                        deferred.resolve(res.rows);
                    }
                    else {
                        deferred.reject();
                    }
                }, function (err) {
                    console.error(err);
                    deferred.reject();
                });
                return deferred.promise;
            },
            deleteLoginDetails() {
                var deferred = $q.defer();
                var query = "Delete FROM loginCache";
                $cordovaSQLite.execute(db, query).then(function (res) {
                    deferred.resolve(res.rows);
                }, function (err) {
                    console.error(err);
                });
                return deferred.promise;
            },
            deleteTag: function (tagId) {
                var deferred = $q.defer();
                console.log("Trying to delete: " + tagId);
                var query = "Delete from tags where tagId='" + tagId + "'";
                $cordovaSQLite.execute(db, query).then(function (res) {
                    console.log("Delete ID -> " + res.insertId);
                    deferred.resolve();
                }, function (err) {
                    console.error(err);
                });
                return deferred.promise;
            },
            insertAttachment: function (tagId, image, device) {
                var deferred = $q.defer();
                var query = "INSERT INTO attachments (tagId, fileName, attachment, comment) VALUES (?,?,?,?)";
                $cordovaSQLite.execute(db, query, [tagId, image.name, image.image, device.comments]).then(function (res) {
                    console.log("INSERT ID -> " + res.insertId);
                    deferred.resolve();
                }, function (err) {
                    console.error(err);
                });
                return deferred.promise;
            },
            insertDevice: function (tagId, deviceId, device, coords, isGps) {
                var deferred = $q.defer();
                var query = "INSERT INTO devices (tagId, deviceId, deviceName, deviceDescription, syncStatus, syncReason, coords) VALUES (?,?,?,?,?,?,?)";
                var deviceName = "";
                var deviceDesc = "";
                if (device) {
                    deviceName = device.name;
                    deviceDesc = device.description;
                }
                var gps = "";
                if (isGps === 1) {
                    gps = coords.latitude + "," + coords.longitude;
                }
                else {
                    gps = coords;//coords.lat() + "," + coords.lng();
                }
                $cordovaSQLite.execute(db, query, [tagId, deviceId, deviceName, deviceDesc, "", "", gps]).then(function (res) {
                    console.log("INSERT ID -> " + res.insertId);
                    deferred.resolve();
                }, function (err) {
                    console.error(err);
                });
                return deferred.promise;
            },
            insertTag: function (taskId, tagId) {
                var deferred = $q.defer();
                var query = "INSERT INTO tags (taskId, tagId) VALUES (?,?)";
                $cordovaSQLite.execute(db, query, [taskId, tagId]).then(function (res) {
                    console.log("INSERT ID -> " + res.insertId);
                    deferred.resolve();
                }, function (err) {
                    console.error(err);
                });
                return deferred.promise;
            },
            getTag: function (tagId) {
                var deferred = $q.defer();
                var query = "SELECT tagId FROM tags where tagId='" + tagId + "'";
                $cordovaSQLite.execute(db, query).then(function (res) {
                    if (res.rows.length > 0) {
                        deferred.resolve(res);
                    }
                    else {
                        deferred.resolve();
                    }
                });
                return deferred.promise;
            },
            updateItemCount: function (taskId, itemCount) {
                var deferred = $q.defer();
                var query = "Update tasks set itemCount='" + itemCount + "' where id='" + taskId + "'";
                $cordovaSQLite.execute(db, query).then(function (res) {
                    console.log("Items updated");
                    deferred.resolve();
                }, function (err) {
                    console.error(err);
                    deferred.reject();
                });
                return deferred.promise;
            },
            insertIntoReplace: function (taskId, oldDetails, newDetails, comment) {
                var deferred = $q.defer();
                var query = "INSERT INTO replaceTable (taskId, oldTag, oldDevice, newTag, newDevice, comment, syncReason) VALUES (?,?,?,?,?,?,?)";
                $cordovaSQLite.execute(db, query, [taskId, oldDetails.tagId, oldDetails.deviceId, newDetails.tagId, newDetails.deviceId, comment, ""]).then(function (res) {
                    console.log("Items updated");
                    deferred.resolve();
                }, function (err) {
                    console.error(err);
                    deferred.reject();
                });
                return deferred.promise;
            },
            selectItemCountForReplace: function (taskId) {
                var deferred = $q.defer();
                var query = "Select * from replaceTable where taskId='" + taskId + "'";
                $cordovaSQLite.execute(db, query).then(function (res) {
                    var count = res.rows.length;
                    console.log("TaskId: " + taskId);
                    console.log("Count: " + count);
                    deferred.resolve(count);
                    //updateItemCount(taskId, count);
                }, function (err) {
                    console.error(err);
                    deferred.resolve(0);
                });
                return deferred.promise;
            },
            getTagsFromReplace: function (taskId) {
                var deferred = $q.defer();
                var query = "Select * FROM replaceTable where taskId='" + taskId + "'";
                $cordovaSQLite.execute(db, query).then(function (res) {
                    if (res.rows.length > 0) {
                        deferred.resolve(res.rows);
                    }
                    else {
                        deferred.resolve([]);
                    }
                }, function (err) {
                    console.error(err);
                    deferred.reject(err);
                });
                return deferred.promise;
            },
            deleteFromReplace: function (tagId) {
                var deferred = $q.defer();
                var query = "Delete FROM replaceTable where newTag='" + tagId + "' OR oldTag='" + tagId + "'";
                $cordovaSQLite.execute(db, query).then(function (res) {
                    deferred.resolve();
                }, function (err) {
                    console.error(err);
                    deferred.reject(err);
                });
                return deferred.promise;
            },
            updateSyncReasonForReplace: function (taskId, tagId, status, syncReason) {
                var deferred = $q.defer();
                var syncStatus = "";
                if (!status) {
                    syncStatus = "Sync failed";
                }
                if (!syncReason || syncReason === "") {
                    syncReason = "Connection failed";
                }
                if (status) {
                    syncReason = "";
                }
                console.log(taskId);
                console.log(tagId);
                console.log(status);
                console.log(syncReason);
                var query = "Update replaceTable set syncReason='" + syncReason + "' where taskId='" + taskId + "' AND newTag='" + tagId + "'";
                $cordovaSQLite.execute(db, query).then(function (res) {
                    console.log("Replace updated");
                    deferred.resolve();
                }, function (err) {
                    console.error(err);
                    deferred.reject();
                });
                return deferred.promise;
            },
            getReasonForReplace: function (taskId) {
                var deferred = $q.defer();
                var query = "Select syncReason FROM replaceTable where taskId='" + taskId + "'";
                $cordovaSQLite.execute(db, query).then(function (res) {
                    var response = "";
                    if (res.rows.length > 0) {
                        for (var i = 0; i < res.rows.length; i++) {
                            if (res.rows.item(i).syncReason) {
                                response = res.rows.item(i).syncReason;
                                break;
                            }
                        }
                    }
                    deferred.resolve(response);
                }, function (err) {
                    console.error(err);
                    deferred.reject(err);
                });
                return deferred.promise;
            }
        }
    });