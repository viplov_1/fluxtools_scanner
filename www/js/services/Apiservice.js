'use strict';

angular.module('starter')
    .service('Apiservice', function Apiservice($http, $q) {
        var parent = "http://onq.io:9000";
        return {
            getFloorPlanImage: function (fpId) {
                console.log("Trying to hit api for" + fpId);
                var deferred = $q.defer();
                var url = parent + "/api/scan/fp/" + fpId;
                $http.get(url, { responseType: "blob" }).then(function (response) {
                    var fr = new FileReader();
                    fr.onload = function () {
                        //console.log(fr.result);
                        //console.log(deferred);
                        deferred.resolve(fr.result);
                    };
                    var items = fr.readAsDataURL(response.data);

                });
                return deferred.promise;
            },
            getAllTasks: function () {
                var deferred = $q.defer();
                var url = parent + "/api/taskInfo";
                $http.get(url).then(function (response) {
                    var items = response.data;
                    deferred.resolve(items);
                },
                    function (error) {
                        deferred.reject(error);
                    });
                return deferred.promise;
            },
            login: function (user) {
                var deferred = $q.defer();
                var url;
                if (!user.host) {
                    url = parent + "/api/login";
                }
                else {
                    url = user.host + "/api/login";
                }
                if (url.indexOf("http") == -1) {
                    url = "http://" + uri;
                }
                $http({
                    method: 'POST',
                    url: url,
                    headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
                    transformRequest: function (obj) {
                        var str = [];
                        for (var p in obj)
                            str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                        return str.join("&");
                    },
                    data: {
                        email: user.email,
                        password: user.password
                    }
                }).then(function (data) {
                    console.log(data);
                    deferred.resolve(data);
                },
                    function (err) {
                        deferred.reject();
                    }
                    );
                return deferred.promise;
            },
            logout: function (user) {
                var deferred = $q.defer();
                var url = parent + "/api/logout";
                $http.get(url).then(function (response) {
                    var items = response.data;
                    deferred.resolve(items);
                });
                return deferred.promise;
            },
            syncWithServer: function (tag, uri, method, value) {
                var deferred = $q.defer();
                console.log(tag);
                var apiOffset = "";
                if (method === "PUT") {
                    if (value === "loctag_info.TagID") {
                        apiOffset = "/" + tag.TagID;
                    }
                    else {
                        apiOffset = "/device/" + tag.DeviceID;
                    }
                }
                if (uri.indexOf("http") == -1) {
                    uri = "http://" + uri;
                }
                $http({
                    url: uri + apiOffset, //"http://requestb.in/sh1txmsh", //
                    method: method,
                    data: JSON.stringify(tag),
                    headers: { 'Content-Type': 'application/json' }
                }).success(function (data, status, headers, config) {
                    console.log("Tag successfully synced: " + status);
                    deferred.resolve();
                    //selectItemCount(tag.TaskID);
                }).error(function (data, status, headers, config) {
                    deferred.reject(data, status, headers, config);
                });
                return deferred.promise;
            },
            syncReplace: function (tag, host) {
                var deferred = $q.defer();
                var uri = host + "/api/scan/replace";
                $http({
                    url: uri,
                    method: "PUT",
                    data: JSON.stringify(tag),
                    headers: { 'Content-Type': 'application/json' }
                }).success(function (data, status, headers, config) {
                    console.log("Tag successfully synced: " + status);
                    deferred.resolve();
                    //selectItemCount(tag.TaskID);
                }).error(function (data, status, headers, config) {
                    deferred.reject(data, status, headers, config);
                });
                return deferred.promise;
            },
            updateGPS: function (tag, host) {
                var deferred = $q.defer();
                var uri = "http://" + host + "/api/mapMarker/gps";
                if (uri.indexOf("http") == -1) {
                    uri = "http://" + uri;
                }
                $http({
                    url: uri,
                    method: "PUT",
                    data: JSON.stringify(tag),
                    headers: { 'Content-Type': 'application/json' }
                }).success(function (data, status, headers, config) {
                    console.log("Tag successfully synced: " + status);
                    deferred.resolve();
                    //selectItemCount(tag.TaskID);
                }).error(function (data, status, headers, config) {
                    deferred.reject(data, status, headers, config);
                });
                return deferred.promise;
            },
            deleteTag: function (tag, comment, host) {
                var deferred = $q.defer();
                var uri = host + "/api/scan/" + tag;
                if (uri.indexOf("http") == -1) {
                    uri = "http://" + uri;
                }
                $http({
                    url: uri,
                    method: "DELETE",
                    data: JSON.stringify({
                        Comment: comment
                    }),
                    headers: { 'Content-Type': 'application/json' }
                }).success(function (data, status, headers, config) {
                    console.log("Tag successfully synced: " + status);
                    deferred.resolve();
                    //selectItemCount(tag.TaskID);
                }).error(function (data, status, headers, config) {
                    deferred.reject(data, status, headers, config);
                });
                return deferred.promise;
            },
            moveTag: function (tag, host) {
                var deferred = $q.defer();
                var uri = host + "/api/scan/control";
                if (uri.indexOf("http") == -1) {
                    uri = "http://" + uri;
                }
                $http({
                    url: uri,
                    method: "PUT",
                    data: tag,
                    headers: { 'Content-Type': 'application/json' }
                }).success(function (data, status, headers, config) {
                    console.log("Tag successfully synced: " + status);
                    deferred.resolve();
                }).error(function (data, status, headers, config) {
                    deferred.reject(data, status, headers, config);
                });
                return deferred.promise;
            }
        }
    });