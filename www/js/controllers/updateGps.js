angular.module('starter.controllers')

    .controller('UpdateGPSCtrl', function ($scope, $rootScope, $state, leafletData, leafletBoundsHelpers, leafletMarkerEvents, $stateParams, Dataservice, $cordovaBarcodeScanner, $cordovaGeolocation, $ionicPopup, $q, $filter, Apiservice) {
        $scope.task = {};
        $scope.current = {
            tagId: ""
        }

        if ($stateParams.taskId) {
            $scope.task.name = $stateParams.taskName;
            $scope.task.id = $stateParams.taskId;
            $scope.task.itemCount = $stateParams.itemCount;
            $scope.task.uri = $stateParams.uri;
            $scope.map = $stateParams.map;
            $rootScope.task = $scope.task;
            $rootScope.map = $stateParams.map;
        }
        else {
            $scope.task = $rootScope.task;
            $scope.current.tagId = $rootScope.tagId;
            $scope.map = $rootScope.map;
        }

        var map = $scope.map.toLowerCase();
        if (map !== 'google' && map !== 'baidu') {

            $scope.recenter = function () {
                $scope.center.lat = 0;
                $scope.center.lng = 0;
            }

            angular.extend($scope, {
                defaults: {
                    scrollWheelZoom: true,
                    crs: 'Simple',
                    maxZoom: 3,
                    minZoom: -4,
                    zoomControl: false
                },
                center: {
                    lat: 0,
                    lng: 0,
                    zoom: -1
                },
                events: {
                    map: {
                        enable: [],
                        logic: 'emit'
                    },
                    markers: {
                        enable: ['click'],
                        logic: 'emit'
                    }
                },
                layers: {
                    baselayers: {
                        overlay: {
                            name: 'Map',
                            type: 'imageOverlay',
                            url: "",
                            bounds: [[-540, -960], [540, 960]],
                            layerParams: {
                                showOnSelector: false,
                                noWrap: true,
                                doRefresh: true
                            }
                        }
                    },
                }
            });

            Dataservice.getPlan($scope.map).then(function (data) {
                $scope.layers.baselayers.overlay.url = data;
                $scope.layers.baselayers.overlay.doRefresh = true;
            }, function (err) {
                Apiservice.getFloorPlanImage($scope.map).then(function (data) {
                    $scope.layers.baselayers.overlay.url = data;
                    $scope.layers.baselayers.overlay.doRefresh = true;
                    Dataservice.insertPlan(map, data);
                });
            });

        } else {

            var options = { timeout: 10000, enableHighAccuracy: true };

            $cordovaGeolocation.getCurrentPosition(options).then(function (position) {
                $scope.center.lat = position.coords.latitude;
                $scope.center.lng = position.coords.longitude;
            }, function (error) {
                console.log("Could not get location");
            });

            $scope.recenter = function () {
                $cordovaGeolocation.getCurrentPosition(options).then(function (position) {
                    $scope.center.lat = position.coords.latitude;
                    $scope.center.lng = position.coords.longitude;
                });
            }

            angular.extend($scope, {
                center: {
                    lat: 25.016813,
                    lng: 121.297666,
                    zoom: 19
                },
                layers: {
                    baselayers: [
                        {
                            name: 'Status',
                            layerType: 'TERRAIN',
                            type: 'google',
                            layerOptions: {
                                attribution: "",
                                minZoom: 17,
                                maxZoom: 19
                            },
                            layerParams: {
                                showOnSelector: false
                            }
                        }
                    ]
                },
                defaults: {
                    scrollWheelZoom: true,
                    maxZoom: 19,
                    minZoom: 17,
                    zoomControl: false
                },
                events: {
                    map: {
                        enable: [],
                        logic: 'emit'
                    },
                    marker: {
                        enable: ['click'],
                        logic: 'emit'
                    }
                }
            });
        }

        $scope.scanTagId = function () {
            $cordovaBarcodeScanner.scan().then(function (imageData) {
                var barcode = imageData.text;
                $scope.current.tagId = barcode.substring(barcode.lastIndexOf("/") + 1);
                console.log("Barcode Format -> " + imageData.format);
                console.log("Cancelled -> " + imageData.cancelled);
            }, function (error) {
                console.log("An error happened -> " + error);
            });
        }

        // function validateTagId(barcode) {
        //     var crc = crc32(barcode.substring(5));
        //     var checkWith = barcode.substring(3, 5);
        //     if (checkWith.toLowerCase() === crc.substring(crc.length - 2).toLowerCase()) {
        //         return true;
        //     }
        //     else {
        //         return false;
        //     }
        // }

        // function makeCRCTable() {
        //     var c;
        //     var crcTable = [];
        //     for (var n = 0; n < 256; n++) {
        //         c = n;
        //         for (var k = 0; k < 8; k++) {
        //             c = ((c & 1) ? (0xEDB88320 ^ (c >>> 1)) : (c >>> 1));
        //         }
        //         crcTable[n] = c;
        //     }
        //     return crcTable;
        // }

        // function crc32(str) {
        //     var crcTable = window.crcTable || (window.crcTable = makeCRCTable());
        //     var crc = 0 ^ (-1);

        //     for (var i = 0; i < str.length; i++) {
        //         crc = (crc >>> 8) ^ crcTable[(crc ^ str.charCodeAt(i)) & 0xFF];
        //     }
        //     var dec = (crc ^ (-1)) >>> 0;
        //     var hexString = dec.toString(16)
        //     return hexString;
        // };

        $scope.saveTag = function () {
            var deferred = $q.defer();
            if ($scope.current.tagId.indexOf("/") !== -1) {
                $scope.current.tagId = $scope.current.tagId.substring($scope.current.tagId.lastIndexOf("/") + 1);
            }
            // var isValid = validateTagId($scope.current.tagId);
            // if (!isValid) {
            //     alert($filter('translate')('INVALIDTAG'));
            //     $scope.current.tagId = "";
            //     return;
            // }

            if ($scope.current.tagId !== "") {
                var currentTags = [];
                Dataservice.getTag($scope.current.tagId).then(function (res) {
                    if (res) {
                        for (var i = 0; i < res.rows.length; i++) {
                            console.log(res.rows);
                            currentTags.push(res.rows.item(i).tagId);
                        }
                    } else {
                        console.log("No results found");
                    }
                    console.log("Current tags: " + currentTags);
                    if (currentTags && currentTags.indexOf($scope.current.tagId) !== -1) {
                        $ionicPopup.show({
                            template: $filter('translate')('TAG_EXISTS'),
                            title: $filter('translate')('DUPLICATETAG'),
                            subTitle: '',
                            scope: $scope,
                            buttons: [{
                                text: $filter('translate')('KEEPOLD'),
                                type: 'button-large button-bar',
                                onTap: function (e) {
                                    clearScope();
                                    deferred.resolve();
                                }
                            },
                            {
                                text: $filter('translate')('USENEW'),
                                type: 'button-large',
                                onTap: function (e) {
                                    deleteTag($scope.current.tagId).then(function () {
                                        Dataservice.insertTag($scope.task.id, $scope.current.tagId).then(function () {
                                            getTagNumber($scope.task.id);
                                        });
                                        if ($scope.center) {
                                            Dataservice.insertDevice($scope.current.tagId, "", {}, $scope.center.lat + "," + $scope.center.lng, 0);
                                            deferred.resolve();
                                        }
                                        else {
                                            $cordovaGeolocation.getCurrentPosition(options).then(function (position) {
                                                Dataservice.insertDevice($scope.current.tagId, "", {}, position.coords, 1);
                                                deferred.resolve();
                                            });
                                        }
                                    });
                                }
                            }]
                        });
                    }
                    else {
                        Dataservice.insertTag($scope.task.id, $scope.current.tagId);
                        if ($scope.center) {
                            Dataservice.insertDevice($scope.current.tagId, "", {}, $scope.center.lat + "," + $scope.center.lng, 0);
                            getTagNumber($scope.task.id);
                            clearScope();
                            deferred.resolve();
                        }
                        else {
                            $cordovaGeolocation.getCurrentPosition(options).then(function (position) {
                                Dataservice.insertDevice($scope.current.tagId, "", {}, position.coords, 1);
                                getTagNumber($scope.task.id);
                                clearScope();
                                deferred.resolve();
                            });
                        }
                    }
                });
            }
            else {
                alert($filter('translate')('MISSINGTAGDEVICE'));
            }
            return deferred.promise;
        }

        function clearScope() {
            $scope.current.tagId = "";
            $rootScope.tagId = "";
        }

        function getTagNumber(taskId) {
            Dataservice.selectItemCount(taskId).then(function (count) {
                $scope.task.itemCount = count;
                $rootScope.task.itemCount = count;
            });
        }

        $scope.goToItemList = function () {
            $rootScope.task = $scope.task;
            if ($scope.current.tagId !== "") {
                $scope.saveTag().then(function () {
                    $state.go("app.itemList");
                });
            }
            else {
                $state.go("app.itemList");
            }
        }

        $scope.goBack = function () {
            $rootScope.itemCount = "";
            $state.go("app.taskList");
        }

        $scope.openMoreInfo = function () {
            $rootScope.task = $scope.task;
            $rootScope.tagId = $scope.current.tagId;
            $state.go("app.moreInfo");
        }

        function deleteTag(tagId) {
            var deferred = $q.defer();
            Dataservice.deleteTag(tagId).then(function () {
                Dataservice.deleteDevices(tagId).then(function () {
                    Dataservice.deleteAttachments(tagId);
                    deferred.resolve();
                });
                Dataservice.deleteFromReplace(tagId);
            });
            return deferred.promise;
        };
    });