angular.module('starter.controllers')

    .controller('MoreInfoCtrl', function ($scope, $rootScope, $state, $stateParams, $cordovaSQLite, $cordovaCamera, $cordovaFile, $ionicPopup, $filter) {
        if ($rootScope.images) {
            $scope.images = $rootScope.images;
            $scope.device = $rootScope.device;
        }
        else {
            $scope.images = [];
            $scope.device = {
                name: "",
                description: "",
                comments: ""
            };
        }
        $scope.selectImage = function () {
            var options = {
                destinationType: Camera.DestinationType.DATA_URL,
                sourceType: Camera.PictureSourceType.PHOTOLIBRARY,
                allowEdit: false,
                encodingType: Camera.EncodingType.JPEG,
                popoverOptions: CameraPopoverOptions,
                saveToPhotoAlbum: false
            };
            $cordovaCamera.getPicture(options).then(function (imageData) {
                var image = "data:image/jpeg;base64," + imageData;
                $scope.images.push({
                    name: "attachment" + $scope.images.length + ".jpg",
                    image: image
                });
            });
        }

        $scope.captureImage = function () {
            var options = {
                destinationType: Camera.DestinationType.DATA_URL,
                sourceType: Camera.PictureSourceType.CAMERA,
                allowEdit: false,
                encodingType: Camera.EncodingType.JPEG,
                popoverOptions: CameraPopoverOptions,
                saveToPhotoAlbum: false
            };
            $cordovaCamera.getPicture(options).then(function (imageData) {
                var image = "data:image/jpeg;base64," + imageData;
                $scope.images.push({
                    name: "attachment" + $scope.images.length + ".jpg",
                    image: image
                });
            });
        }

        $scope.deleteImagePopup = function (imageNo) {
            $ionicPopup.show({
                template: $filter('translate')('DELETE_CONFIRM'),
                title: 'Delete Image',
                subTitle: '',
                scope: $scope,
                buttons: [{
                    text: $filter('translate')('YES'),
                    type: 'button-large button-bar button-dark',
                    onTap: function (e) {
                        $scope.images.splice(imageNo);
                    }
                },
                {
                    text: $filter('translate')('NO'),
                    type: 'button-large'
                }]
            });
        }

        $scope.goBack = function () {
            $rootScope.images = $scope.images;
            $rootScope.device = $scope.device;
            console.log($rootScope.device);
            $state.go("app.mapScan");
        }

    });