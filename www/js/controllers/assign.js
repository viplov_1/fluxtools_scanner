angular.module('starter.controllers')

    .controller('AssignCtrl', function ($scope, $rootScope, $state, leafletData, leafletBoundsHelpers, leafletMarkerEvents, $stateParams, $cordovaSQLite, $cordovaBarcodeScanner, $cordovaGeolocation, $ionicPopup, $q, $filter, Apiservice, Dataservice, $timeout) {
        $scope.task = {};
        $scope.current = {
            tagId: "",
            deviceId: ""
        }
        //$scope.current.tagId = "";
        //$scope.current.deviceId = "";
        if ($stateParams.taskId) {
            $scope.task.name = $stateParams.taskName;
            $scope.task.id = $stateParams.taskId;
            $scope.task.itemCount = $stateParams.itemCount;
            $scope.task.uri = $stateParams.uri;
            $scope.map = $stateParams.map;
            $rootScope.task = $scope.task;
            $rootScope.map = $stateParams.map;
        }
        else {
            $scope.task = $rootScope.task;
            $scope.current.tagId = $rootScope.tagId;
            $scope.current.deviceId = $rootScope.deviceId;
            $scope.map = $rootScope.map;
        }

        $scope.scanTagId = function () {
            $cordovaBarcodeScanner.scan().then(function (imageData) {
                //alert(imageData.text);
                var barcode = imageData.text;
                $scope.current.tagId = barcode.substring(barcode.lastIndexOf("/") + 1);
                console.log("Barcode Format -> " + imageData.format);
                console.log("Cancelled -> " + imageData.cancelled);
            }, function (error) {
                console.log("An error happened -> " + error);
            });
        }

        function validateTagId(barcode) {
            var crc = crc32(barcode.substring(5));
            var checkWith = barcode.substring(3, 5);
            if (checkWith.toLowerCase() === crc.substring(crc.length - 2).toLowerCase()) {
                return true;
            }
            else {
                return false;
            }
        }

        function makeCRCTable() {
            var c;
            var crcTable = [];
            for (var n = 0; n < 256; n++) {
                c = n;
                for (var k = 0; k < 8; k++) {
                    c = ((c & 1) ? (0xEDB88320 ^ (c >>> 1)) : (c >>> 1));
                }
                crcTable[n] = c;
            }
            return crcTable;
        }

        function crc32(str) {
            var crcTable = window.crcTable || (window.crcTable = makeCRCTable());
            var crc = 0 ^ (-1);

            for (var i = 0; i < str.length; i++) {
                crc = (crc >>> 8) ^ crcTable[(crc ^ str.charCodeAt(i)) & 0xFF];
            }
            var dec = (crc ^ (-1)) >>> 0;
            var hexString = dec.toString(16)
            return hexString;
        };

        $scope.scanDeviceId = function () {
            $cordovaBarcodeScanner.scan().then(function (imageData) {
                var barcode = imageData.text;
                $scope.current.deviceId = barcode;
                console.log("Barcode Format -> " + imageData.format);
                console.log("Cancelled -> " + imageData.cancelled);
            }, function (error) {
                console.log("An error happened -> " + error);
            });
        }

        $scope.saveTag = function () {
            var deferred = $q.defer();
            if ($scope.current.tagId.indexOf("/") !== -1) {
                $scope.current.tagId = $scope.current.tagId.substring($scope.current.tagId.lastIndexOf("/") + 1);
            }
            var isValid = validateTagId($scope.current.tagId);
            if (!isValid) {
                alert($filter('translate')('INVALIDTAG'));
                $scope.current.tagId = "";
                return;
            }
            var isHex = $scope.current.deviceId.match("[0-9A-Fa-f]{8}");
            if (!isHex) { //|| $scope.current.deviceId.length !== 8) {
                alert($filter('translate')('INVALIDDEVICE'));
                $scope.current.deviceId = "";
                return;
            }
            if ($scope.current.tagId !== "" && $scope.current.deviceId !== "") {
                var currentTags = [];
                Dataservice.getTag($scope.current.tagId).then(function (res) {
                    if (res) {
                        for (var i = 0; i < res.rows.length; i++) {
                            console.log(res.rows);
                            currentTags.push(res.rows.item(i).tagId);
                        }
                    } else {
                        console.log("No results found");
                    }
                    console.log("Current tags: " + currentTags);
                    if (currentTags && currentTags.indexOf($scope.current.tagId) !== -1) {
                        $ionicPopup.show({
                            template: $filter('translate')('TAG_EXISTS'),
                            title: $filter('translate')('DUPLICATETAG'),
                            subTitle: '',
                            scope: $scope,
                            buttons: [{
                                text: $filter('translate')('KEEPOLD'),
                                type: 'button-large button-bar',
                                onTap: function (e) {
                                    clearScope();
                                    deferred.resolve();
                                }
                            },
                            {
                                text: $filter('translate')('USENEW'),
                                type: 'button-large',
                                onTap: function (e) {
                                    deleteTag($scope.current.tagId).then(function () {
                                        Dataservice.insertTag($scope.task.id, $scope.current.tagId).then(function () {
                                            getTagNumber($scope.task.id);
                                        });
                                        Dataservice.insertDevice($scope.current.tagId, $scope.current.deviceId, $rootScope.device, "", 0);
                                        clearScope();
                                        deferred.resolve();
                                    });
                                }
                            }]
                        });
                    }
                    else {
                        Dataservice.insertTag($scope.task.id, $scope.current.tagId);
                        Dataservice.insertDevice($scope.current.tagId, $scope.current.deviceId, $rootScope.device, "", 0);
                        getTagNumber($scope.task.id);
                        clearScope();
                        deferred.resolve();
                    }
                });
            }
            else {
                alert($filter('translate')('MISSINGTAGDEVICE'));
            }
            return deferred.promise;
        }

        function clearScope() {
            $scope.current.tagId = "";
            $scope.current.deviceId = "";
            $rootScope.tagId = "";
            $rootScope.deviceId = "";
            $scope.focusInTag();
            $rootScope.device = {};
            $rootScope.images = [];
        }

        $scope.focusDevice = function () {
            console.log($scope.focusTag);
            if ($scope.focusTag) {
                $scope.focusTag = false;
                $scope.focusInput = true;
            }
            else {
                $scope.focusTag = true;
                $timeout(function () {
                    $scope.focusTag = false;
                }, 500);
                $scope.focusInput = false;
                $timeout(function () {
                    $scope.focusInput = true;
                }, 500);
            }
        }

        $scope.focusInTag = function () {
            console.log($scope.focusTag);
            if (!$scope.focusTag) {
                $scope.focusInput = false;
                $scope.focusTag = true;
            }
            else {
                $scope.focusTag = false;
                $timeout(function () {
                    $scope.focusTag = true;
                }, 500);
                $scope.focusInput = true;
                $timeout(function () {
                    $scope.focusInput = false;
                }, 500);

            }
        }

        function getTagNumber(taskId) {
            Dataservice.selectItemCount(taskId).then(function (count) {
                $scope.task.itemCount = count;
                $rootScope.task.itemCount = count;
            });
        }

        $scope.goToItemList = function () {
            $rootScope.task = $scope.task;
            if ($scope.current.tagId !== "" || $scope.current.deviceId !== "") {
                $scope.saveTag().then(function () {
                    $state.go("app.itemList");
                });
            }
            else {
                $state.go("app.itemList");
            }
        }

        $scope.goBack = function () {
            $rootScope.images = [];
            $rootScope.device = {};
            $rootScope.itemCount = "";
            $state.go("app.taskList");
        }

        function deleteTag(tagId) {
            var deferred = $q.defer();
            Dataservice.deleteTag(tagId).then(function () {
                Dataservice.deleteDevices(tagId).then(function () {
                    Dataservice.deleteAttachments(tagId);
                    deferred.resolve();
                });
                Dataservice.deleteFromReplace(tagId);
            });
            return deferred.promise;
        };

        $scope.isDeviceFocus = false;
        $scope.changeFocus = function () {
            $scope.isDeviceFocus = true;
        };
    });