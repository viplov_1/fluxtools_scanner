'use strict';

angular.module('starter.controllers')

    .controller('ItemListCtrl', function ($scope, $rootScope, $state, $stateParams, Dataservice, Apiservice, $ionicPopup, $q, $filter) {
        $scope.task = $rootScope.task;
        $scope.data = {};
        refreshItems();
        function refreshItems() {
            $scope.items = [];
            if ($rootScope.type === "replace") {
                Dataservice.getTagsFromReplace($scope.task.id).then(function (rows) {
                    for (var i = 0; i < rows.length; i++) {
                        var syncStatus = "";
                        if (rows.item(i).syncReason) {
                            syncStatus = "Sync failed";
                        }
                        console.log(rows);
                        $scope.items.push({
                            id: rows.item(i).newTag,
                            deviceId: rows.item(i).newDevice,
                            name: "",
                            syncStatus: syncStatus,
                            syncReason: rows.item(i).syncReason,
                            desc: "Replace for " + rows.item(i).oldTag
                        });
                    }
                    $rootScope.task.itemCount = rows.length;
                    updateItemCount($scope.task.id, rows.length);
                });
            }
            else {
                selectTags($rootScope.task.id);
            }
        }

        function selectTags(taskId) {
            var currentTags = [];
            Dataservice.getTags(taskId).then(function (tags) {
                for (var i = 0; i < tags.length; i++) {
                    selectTagRow(tags[i]);
                }
                $rootScope.task.itemCount = tags.length;
                updateItemCount(taskId, tags.length);
            }, function (err) {
                console.error(err);
            });
        }

        function selectTagRow(tagId) {
            var tagDetails = {};
            Dataservice.getDevices(tagId).then(function (rows) {
                if (rows.length > 0) {
                    tagDetails = {
                        id: tagId,
                        deviceId: rows.item(0).deviceId,
                        name: rows.item(0).deviceName,
                        syncStatus: rows.item(0).syncStatus,
                        syncReason: rows.item(0).syncReason,
                        desc: rows.item(0).deviceDescription
                    };
                } else {
                    console.log("No results found");
                }
                getAttachmentNo(tagDetails);
            }, function (err) {
                console.error(err);
            });
        }

        function getAttachmentNo(tag) {
            Dataservice.getAttachments(tag.id).then(function (attachments) {
                console.log("Items updated");
                tag.attachments = attachments.length;
            }, function (err) {
                console.error(err);
            });
            $scope.items.push(tag);
        }

        function updateItemCount(taskId, itemCount) {
            Dataservice.updateItemCount(taskId, itemCount);
        }

        $scope.goBack = function () {
            $rootScope.tagId = "";
            $rootScope.deviceId = "";
            $rootScope.device = {};
            $rootScope.images = [];
            if ($rootScope.type) {
                switch ($rootScope.type) {
                    case "GPS":
                        $state.go("app.updategps");
                        break;
                    case "delete":
                        $state.go("app.deletetag");
                        break;
                    case "replace":
                        $state.go("app.replacetag");
                        break;
                    case "lite":
                        $state.go("app.lite");
                        break;
                    case "move":
                        $state.go("app.move");
                        break;
                    case "assign":
                        $state.go("app.assign");
                        break;
                    default: $state.go("app.mapScan");
                }
            }
            else {
                $state.go("app.mapScan");
            }
        }

        $scope.showTag = function () {
            var type = ["lite"];
            if (type.indexOf($rootScope.type) !== -1)
                return false;
            return true;
        }
        $scope.showDevice = function () {
            var type = ["replace", "GPS", "delete", "move"];
            if (type.indexOf($rootScope.type) !== -1)
                return false;
            return true;
        }
        $scope.showName = function () {
            var type = ["replace", "GPS", "delete", "move"];
            if (type.indexOf($rootScope.type) !== -1)
                return false;
            return true;
        }
        $scope.showDesc = function () {
            var type = ["GPS", "assign", "move"];
            if (type.indexOf($rootScope.type) !== -1)
                return false;
            return true;
        }
        $scope.showAttachment = function () {
            var type = ["replace", "GPS", "delete", "assign", "move"];
            if (type.indexOf($rootScope.type) !== -1)
                return false;
            return true;
        }

        $scope.user = {
            email: "",
            password: "",
            host: ""
        }

        $scope.syncWrap = function () {
            var loginRequired = false;
            var task = $scope.task;
            if (task.type === "replace" || task.type === "delete") {
                if (task.itemCount > 0) {
                    loginRequired = true;
                }
            }
            if (loginRequired) {
                Dataservice.getLoginDetails().then(function (rows) {
                    $scope.user.email = rows.item(0).email;
                    $scope.user.host = rows.item(0).provisioning;
                },
                    function () {
                        $scope.user.email = "";
                        $scope.user.host = "prov1.8maps.com";
                    }
                );
                $ionicPopup.show({
                    templateUrl: "templates/login.html",
                    title: $filter('translate')('LOGIN'),
                    scope: $scope,
                    buttons: [{
                        text: $filter('translate')('LOGIN'),
                        type: 'button-large button-bar button-dark',
                        onTap: function (e) {
                            Dataservice.deleteLoginDetails();
                            Dataservice.insertLoginDetails($scope.user.email, $scope.user.host);
                            Apiservice.login($scope.user).then(function (response) {
                                $scope.user.password = "";
                                //$scope.showProgress();
                                $scope.syncNow().then(function () {
                                    Apiservice.logout();
                                    //$scope.hideProgress();
                                    refreshItems();
                                }
                                );
                            }, function (err) {
                                Apiservice.logout();
                                $scope.syncWrap();
                                $scope.user.password = "";
                            });
                        }
                    },
                    {
                        text: $filter('translate')('CANCEL'),
                        type: 'button-large button-bar button-dark'
                    }
                    ]
                });
            }
            else {
                //$scope.showProgress();
                $scope.syncNow().then(function () {
                    //$scope.hideProgress();
                    refreshItems();
                });
            }
        }

        $scope.syncNow = function () {
            var deferred = $q.defer();
            var host = "prov1.8maps.com";
            if ($scope.user.host) {
                host = $scope.user.host;
            }
            var count = 0;
            $scope.task.type = $rootScope.type;
            if ($scope.task.type === "replace") {
                Dataservice.getTagsFromReplace($scope.task.id).then(function (replaceTags) {
                    var syncReplaceTags = [];
                    for (var i = 0; i < replaceTags.length; i++) {
                        var replaceObj = {
                            OLDTagID: replaceTags.item(i).oldTag,
                            OLDDeviceID: replaceTags.item(i).oldDevice,
                            TagID: replaceTags.item(i).newTag,
                            DeviceID: replaceTags.item(i).newDevice,
                            Comment: replaceTags.item(i).comment
                        }
                        syncReplaceTags.push(replaceObj);
                    }
                    syncReplace($scope.task.id, syncReplaceTags, host).then(function () {
                        count++;
                        deferred.resolve();
                    },
                        function () {
                            Dataservice.updateSyncStatus($scope.task.id, "");
                            deferred.resolve();
                        }
                    );
                });
            }
            else {
                Dataservice.getTags($scope.task.id).then(function (tags) {
                    console.log(tags);
                    syncTags($scope.task, tags, host).then(function () {
                        deferred.resolve();
                    },
                        function (err) {
                            deferred.resolve();
                        }
                    );
                },
                    function (err) {
                        deferred.resolve();
                    }
                );
            }

            return deferred.promise;
        };

        function syncReplace(taskId, currentTags, host) {
            var deferred = $q.defer();
            var synced = 0;
            console.log(currentTags);
            if (currentTags.length === 0) {
                deferred.resolve();
            }
            for (var i = 0; i < currentTags.length; i++) {
                (function (i) {
                    Apiservice.syncReplace(currentTags[i], host).then(function (data) {
                        deleteTags(currentTags[i]).then(function () {
                            synced++;
                            if (synced === currentTags.length) {
                                deferred.resolve();
                            }
                        })
                    },
                        function (err) {
                            Dataservice.updateSyncStatus(taskId, "").then(function () {
                                Dataservice.updateSyncReasonForReplace(taskId, currentTags[i].TagID, "", "Connection failed");
                                synced++;
                                if (synced === currentTags.length) {
                                    deferred.resolve();
                                }
                            });
                        });
                })(i);
            }
            return deferred.promise;
        }

        function syncTags(task, currentTags, host) {
            var deferred = $q.defer();
            var synced = 0;
            console.log(currentTags);
            if (currentTags.length === 0) {
                deferred.resolve();
            }

            for (var i = 0; i < currentTags.length; i++) {
                (function (i) {
                    var tagId = currentTags[i];
                    Dataservice.getDevices(tagId).then(function (rows) {
                        if (rows.length > 0) {
                            switch (task.type) {
                                case "delete":
                                    var desc = rows.item(0).deviceDescription;
                                    Apiservice.deleteTag(tagId, desc, host).then(function () {
                                        synced++;
                                        if (synced === currentTags.length) {
                                            deferred.resolve();
                                        }
                                    },
                                        function (err) {
                                            synced++;
                                            if (synced === currentTags.length) {
                                                deferred.resolve();
                                            }
                                            updateSyncStatus(task.id, tagId, "", "Connection failed");
                                        }
                                    );
                                    break;
                                case "GPS":
                                    var tag = {
                                        ID: tagId,
                                        GPS: rows.item(0).coords
                                    }
                                    Apiservice.updateGPS(tag, host).then(function () {
                                        synced++;
                                        if (synced === currentTags.length) {
                                            deferred.resolve();
                                        }
                                    },
                                        function (err) {
                                            synced++;
                                            if (synced === currentTags.length) {
                                                deferred.resolve();
                                            }
                                            updateSyncStatus(task.id, tagId, "", "Connection failed");
                                        }
                                    );
                                    break;
                                case "move":
                                    var tag = {
                                        taskid: task.id,
                                        Tag: tagId
                                    }
                                    Apiservice.moveTag(tag, host).then(function () {
                                        synced++;
                                        if (synced === currentTags.length) {
                                            deferred.resolve();
                                        }
                                    },
                                        function (err) {
                                            synced++;
                                            if (synced === currentTags.length) {
                                                deferred.resolve();
                                            }
                                            updateSyncStatus(task.id, tagId, "", "Connection failed");
                                        }
                                    );
                                    break;
                                case "assign":
                                    var tag = {
                                        TaskID: task.id,
                                        TagID: tagId,
                                        DeviceID: rows.item(0).deviceId
                                    };
                                    syncWithServer(tag, task.uri, "POST", "").then(function () {
                                        synced++;
                                        console.log(synced);
                                        if (synced === currentTags.length) {
                                            deferred.resolve();
                                        }
                                    });
                                    break;
                                case "lite":
                                case "install":
                                default:
                                    var tag = {
                                        TaskID: task.id,
                                        TagID: tagId,
                                        DeviceID: rows.item(0).deviceId,
                                        GPS: rows.item(0).coords,
                                        Name: rows.item(0).deviceName,
                                        Description: rows.item(0).deviceDescription,
                                        Attachment: []
                                    };
                                    console.log(tag);
                                    Dataservice.getAttachments(tag.TagID).then(function (attachments) {
                                        tag.Attachment = attachments;
                                        syncWithServer(tag, task.uri, "POST", "").then(function () {
                                            synced++;
                                            console.log(synced);
                                            if (synced === currentTags.length) {
                                                deferred.resolve();
                                            }
                                        });
                                    });
                                    break;
                            }
                        }
                    });
                })(i);
            }
            return deferred.promise;
        }

        function syncWithServer(tag, uri, method, value) {
            var deferred = $q.defer();
            Apiservice.syncWithServer(tag, uri, method, value).then(function (response) {
                console.log(tag);
                updateSyncStatus(tag.TaskID, tag.TagID, true, "");
                deleteTags(tag.TaskID, tag.TagID, false).then(function () {
                    deferred.resolve();
                });
            }, function (data, status, headers, config) {
                var failureReasonObject = data;
                console.log(failureReasonObject);
                if (failureReasonObject && failureReasonObject.length >= 1) {
                    var failureReason = failureReasonObject[0].type;
                    var failureValue = failureReasonObject[0].value;
                    var append = "";
                    var prepend = "";
                    if (failureValue === "loctag_info.TagID") {
                        append = " tag " + tag.TagID;
                        prepend = "Tag";
                    }
                    else {
                        append = " device " + tag.DeviceID;
                        prepend = "Device";
                    }
                    if (failureReason === "ERROR_MESSAGE.IS_DULPICATE") {
                        if ($rootScope.futureSelection === "") {
                            $ionicPopup.show({
                                template: prepend + '{{\'TAG_SYNCED\' | translate}}<br/><ion-checkbox ng-model="data.isChecked">{{\'APPLYALL\' | translate}}</ion-checkbox>',
                                title: $filter('translate')('DUPLICATETAG') + append,
                                subTitle: '',
                                scope: $scope,
                                buttons: [{
                                    text: $filter('translate')('OVERWRITE'),
                                    type: 'button button-bar',
                                    onTap: function (e) {
                                        if ($scope.data.isChecked) {
                                            $rootScope.futureSelection = "ow";
                                        }
                                        syncWithServer(tag, uri, "PUT", failureValue).then(function () {
                                            deferred.resolve();
                                        });
                                    }
                                },
                                {
                                    text: $filter('translate')('REMOVE'),
                                    type: 'button',
                                    onTap: function (e) {
                                        if ($scope.data.isChecked) {
                                            $rootScope.futureSelection = "r";
                                        }
                                        deleteTags(tag.TaskID, tag.TagID, false).then(function () {
                                            deferred.resolve();
                                        });
                                        console.log("syncWithServer: Resolved");
                                    }
                                }]
                            });
                        }
                        else {
                            if ($rootScope.futureSelection === "ow") {
                                syncWithServer(tag, uri, "PUT", failureValue).then(function () {
                                    deferred.resolve();
                                });
                            }
                            else {
                                deleteTags(tag.TaskID, TagID, false).then(function () {
                                    deferred.resolve();
                                });
                            }
                        }
                    }
                    else {
                        var reason = "";
                        if (data && data.length >= 1) {
                            reason = getReasonMessage(data[0].type, data[0].value);
                        }
                        if (!reason) {
                            reason = "Not found";
                        }
                        console.log(tag);
                        updateSyncStatus(tag.TaskID, tag.TagID, false, reason).then(function () {
                            deferred.resolve();
                        });
                        deferred.resolve();
                        console.log("syncWithServer: Resolved");
                    }
                }
                else {
                    var reason = "";
                    if (data && data.length >= 1) {
                        reason = getReasonMessage(data[0].type, data[0].value);
                    }
                    if (!reason) {
                        reason = "Not found";
                    }
                    console.log(reason);
                    updateSyncStatus(tag.TaskID, tag.TagID, reason).then(function () {
                        deferred.resolve();
                    });
                    console.log("syncWithServer: Resolved");
                }
            }
            );
            return deferred.promise;
        }

        function getReasonMessage(reasonType, reasonValue) {
            switch (reasonType) {
                case "ERROR_MESSAGE.NOT_EXIST":
                    if (reasonValue === "loctag_info.TagID") {
                        return "Update tag";
                    }
                    else {
                        return "Create and update tag";
                    }
                    break;
                case "ERROR_MESSAGE.IS_DULPICATE":
                    if (reasonValue === "loctag_info.TagID") {
                        return "Create tag";
                    }
                    else {
                        return "Update tag";
                    }
                    break;
                case "ERROR_MESSAGE.PERMISSION_DENIED":
                    return "Update tag and update device";
            }
        }

        function updateSyncStatus(taskId, tagId, status, syncReason) {
            var deferred = $q.defer();
            Dataservice.updateSyncStatus(taskId, status).then(function (res) {
                if (!status) {
                    Dataservice.updateSyncReason(tagId, status, syncReason);
                }
                deferred.resolve();
            });
            return deferred.promise;
        }

        function deleteTags(taskId, tagId, isClear) {
            var deferred = $q.defer();
            Dataservice.deleteTags(taskId, tagId).then(function () {
                Dataservice.deleteDevices(tagId).then(function () {
                    if (isClear) {
                        $scope.tasks = [];
                    }
                    Dataservice.deleteAttachments(tagId);
                    deferred.resolve();
                });
                Dataservice.deleteFromReplace(tagId);
            });
            return deferred.promise;
        }
    });