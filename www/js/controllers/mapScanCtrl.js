angular.module('starter.controllers')

    .controller('MapScanCtrl', function ($scope, $rootScope, $state, leafletData, leafletBoundsHelpers, leafletMarkerEvents, $stateParams, $cordovaSQLite, $cordovaBarcodeScanner, $cordovaGeolocation, $ionicPopup, $q, $filter, Apiservice, Dataservice, $timeout) {
        $scope.task = {};
        $scope.current = {
            tagId: "",
            deviceId: ""
        }
        //$scope.current.tagId = "";
        //$scope.current.deviceId = "";
        if ($stateParams.taskId) {
            $scope.task.name = $stateParams.taskName;
            $scope.task.id = $stateParams.taskId;
            $scope.task.itemCount = $stateParams.itemCount;
            $scope.task.uri = $stateParams.uri;
            $scope.map = $stateParams.map;
            $rootScope.task = $scope.task;
            $rootScope.map = $stateParams.map;
        }
        else {
            $scope.task = $rootScope.task;
            $scope.current.tagId = $rootScope.tagId;
            $scope.current.deviceId = $rootScope.deviceId;
            $scope.map = $rootScope.map;
        }

        console.log($rootScope.images);
        console.log($rootScope.device);
        console.log($stateParams);
        var map = $scope.map.toLowerCase();
        if (map !== 'google' && map !== 'baidu') {

            $scope.recenter = function () {
                $scope.center.lat = 0;
                $scope.center.lng = 0;
            }

            angular.extend($scope, {
                defaults: {
                    scrollWheelZoom: true,
                    crs: 'Simple',
                    maxZoom: 3,
                    minZoom: -4,
                    zoomControl: false
                },
                center: {
                    lat: 0,
                    lng: 0,
                    zoom: -1
                },
                events: {
                    map: {
                        enable: [],
                        logic: 'emit'
                    },
                    markers: {
                        enable: ['click'],
                        logic: 'emit'
                    }
                },
                layers: {
                    baselayers: {
                        overlay: {
                            name: 'Map',
                            type: 'imageOverlay',
                            url: "",
                            bounds: [[-540, -960], [540, 960]],
                            layerParams: {
                                showOnSelector: false,
                                noWrap: true,
                                doRefresh: true
                            }
                        }
                    },
                }
            });

            Dataservice.getPlan($scope.map).then(function (data) {
                $scope.layers.baselayers.overlay.url = data;
                $scope.layers.baselayers.overlay.doRefresh = true;
            }, function (err) {
                Apiservice.getFloorPlanImage(err).then(function (data) {
                    $scope.layers.baselayers.overlay.url = data;
                    $scope.layers.baselayers.overlay.doRefresh = true;
                    Dataservice.insertPlan(err, data);
                });
            });

        } else {

            var options = { timeout: 10000, enableHighAccuracy: true };

            $cordovaGeolocation.getCurrentPosition(options).then(function (position) {
                $scope.center.lat = position.coords.latitude;
                $scope.center.lng = position.coords.longitude;
            }, function (error) {
                console.log("Could not get location");
            });

            $scope.recenter = function () {
                // if (google) {
                $cordovaGeolocation.getCurrentPosition(options).then(function (position) {
                    // var latLng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
                    // $scope.map.setCenter(latLng)
                    $scope.center.lat = position.coords.latitude;
                    $scope.center.lng = position.coords.longitude;
                });
                // }
            }

            angular.extend($scope, {
                center: {
                    lat: 25.016813,
                    lng: 121.297666,
                    zoom: 19
                },
                layers: {
                    baselayers: [
                        {
                            name: 'Status',
                            layerType: 'TERRAIN',
                            type: 'google',
                            layerOptions: {
                                attribution: "",
                                minZoom: 17,
                                maxZoom: 19
                            },
                            layerParams: {
                                showOnSelector: false
                            }
                        }
                    ]
                },
                defaults: {
                    scrollWheelZoom: true,
                    maxZoom: 19,
                    minZoom: 17,
                    zoomControl: false
                },
                events: {
                    map: {
                        enable: [],
                        logic: 'emit'
                    },
                    marker: {
                        enable: ['click'],
                        logic: 'emit'
                    }
                }
            });
        }

        $scope.scanTagId = function () {
            $cordovaBarcodeScanner.scan().then(function (imageData) {
                //alert(imageData.text);
                var barcode = imageData.text;
                $scope.current.tagId = barcode.substring(barcode.lastIndexOf("/") + 1);
                console.log("Barcode Format -> " + imageData.format);
                console.log("Cancelled -> " + imageData.cancelled);
            }, function (error) {
                console.log("An error happened -> " + error);
            });
        }

        function validateTagId(barcode) {
            var crc = crc32(barcode.substring(5));
            var checkWith = barcode.substring(3, 5);
            if (checkWith.toLowerCase() === crc.substring(crc.length - 2).toLowerCase()) {
                return true;
            }
            else {
                return false;
            }
        }

        function makeCRCTable() {
            var c;
            var crcTable = [];
            for (var n = 0; n < 256; n++) {
                c = n;
                for (var k = 0; k < 8; k++) {
                    c = ((c & 1) ? (0xEDB88320 ^ (c >>> 1)) : (c >>> 1));
                }
                crcTable[n] = c;
            }
            return crcTable;
        }

        function crc32(str) {
            var crcTable = window.crcTable || (window.crcTable = makeCRCTable());
            var crc = 0 ^ (-1);

            for (var i = 0; i < str.length; i++) {
                crc = (crc >>> 8) ^ crcTable[(crc ^ str.charCodeAt(i)) & 0xFF];
            }
            var dec = (crc ^ (-1)) >>> 0;
            var hexString = dec.toString(16)
            return hexString;
        };

        $scope.scanDeviceId = function () {
            $cordovaBarcodeScanner.scan().then(function (imageData) {
                var barcode = imageData.text;
                $scope.current.deviceId = barcode;
                console.log("Barcode Format -> " + imageData.format);
                console.log("Cancelled -> " + imageData.cancelled);
            }, function (error) {
                console.log("An error happened -> " + error);
            });
        }

        // $scope.doNext = function () {
        //     $scope.saveTag().then(function () {
        //         $timeout(function () {
        //             console.log("Now moving");
        //             $scope.focusTag = true;
        //         }, 2000);
        //     });
        //     $scope.focusTag = true;
        // }

        $scope.saveTag = function () {

            var deferred = $q.defer();
            if ($scope.current.tagId.indexOf("/") !== -1) {
                $scope.current.tagId = $scope.current.tagId.substring($scope.current.tagId.lastIndexOf("/") + 1);
            }
            var isValid = validateTagId($scope.current.tagId);
            if (!isValid) {
                alert($filter('translate')('INVALIDTAG'));
                $scope.current.tagId = "";
                return;
            }
            var isHex = $scope.current.deviceId.match("[0-9A-Fa-f]{8}");
            if (!isHex) { //|| $scope.current.deviceId.length !== 8) {
                alert($filter('translate')('INVALIDDEVICE'));
                $scope.current.deviceId = "";
                return;
            }
            if ($scope.current.tagId !== "" && $scope.current.deviceId !== "") {
                var currentTags = [];
                Dataservice.getTag($scope.current.tagId).then(function (res) {
                    if (res) {
                        for (var i = 0; i < res.rows.length; i++) {
                            console.log(res.rows);
                            currentTags.push(res.rows.item(i).tagId);
                        }
                    } else {
                        console.log("No results found");
                    }
                    console.log("Current tags: " + currentTags);
                    if (currentTags && currentTags.indexOf($scope.current.tagId) !== -1) {
                        $ionicPopup.show({
                            template: $filter('translate')('TAG_EXISTS'),
                            title: $filter('translate')('DUPLICATETAG'),
                            subTitle: '',
                            scope: $scope,
                            buttons: [{
                                text: $filter('translate')('KEEPOLD'),
                                type: 'button-large button-bar',
                                onTap: function (e) {
                                    clearScope();
                                    deferred.resolve();
                                }
                            },
                            {
                                text: $filter('translate')('USENEW'),
                                type: 'button-large',
                                onTap: function (e) {
                                    deleteTag($scope.current.tagId).then(function () {
                                        Dataservice.insertTag($scope.task.id, $scope.current.tagId).then(function () {
                                            getTagNumber($scope.task.id);
                                        });
                                        if ($scope.center) {
                                            Dataservice.insertDevice($scope.current.tagId, $scope.current.deviceId, $rootScope.device, $scope.center.lat + "," + $scope.center.lng, 0);
                                            if ($rootScope.images) {
                                                for (var i = 0; i < $rootScope.images.length; i++) {
                                                    Dataservice.insertAttachment($scope.current.tagId, $rootScope.images[i], $rootScope.device);
                                                }
                                            }
                                            clearScope();
                                            deferred.resolve();
                                        }
                                        else {
                                            $cordovaGeolocation.getCurrentPosition(options).then(function (position) {
                                                Dataservice.insertDevice($scope.current.tagId, $scope.current.deviceId, $rootScope.device, position.coords, 1);
                                                if ($rootScope.images) {
                                                    for (var i = 0; i < $rootScope.images.length; i++) {
                                                        Dataservice.insertAttachment($scope.current.tagId, $rootScope.images[i], $rootScope.device);
                                                    }
                                                }
                                                clearScope();
                                                deferred.resolve();
                                            });
                                        }
                                    });
                                }
                            }]
                        });
                    }
                    else {
                        Dataservice.insertTag($scope.task.id, $scope.current.tagId);
                        if ($scope.center) {
                            Dataservice.insertDevice($scope.current.tagId, $scope.current.deviceId, $rootScope.device, $scope.center.lat + "," + $scope.center.lng, 0);
                            if ($rootScope.images) {
                                for (var i = 0; i < $rootScope.images.length; i++) {
                                    Dataservice.insertAttachment($scope.current.tagId, $rootScope.images[i], $rootScope.device);
                                }
                            }
                            getTagNumber($scope.task.id);
                            clearScope();
                            deferred.resolve();
                        }
                        else {
                            $cordovaGeolocation.getCurrentPosition(options).then(function (position) {
                                Dataservice.insertDevice($scope.current.tagId, $scope.current.deviceId, $rootScope.device, position.coords, 1);
                                if ($rootScope.images) {
                                    for (var i = 0; i < $rootScope.images.length; i++) {
                                        Dataservice.insertAttachment($scope.current.tagId, $rootScope.images[i], $rootScope.device);
                                    }
                                }
                                getTagNumber($scope.task.id);
                                clearScope();
                                deferred.resolve();
                            });
                        }
                    }
                });
            }
            else {
                alert($filter('translate')('MISSINGTAGDEVICE'));
            }
            return deferred.promise;
        }

        $scope.focusDevice = function () {
            console.log($scope.focusTag);
            if ($scope.focusTag) {
                $scope.focusTag = false;
                $scope.focusInput = true;
            }
            else {
                $scope.focusTag = true;
                $timeout(function () {
                    $scope.focusTag = false;
                }, 500);
                $scope.focusInput = false;
                $timeout(function () {
                    $scope.focusInput = true;
                }, 500);
            }
        }

        $scope.focusInTag = function () {
            console.log($scope.focusTag);
            if (!$scope.focusTag) {
                $scope.focusInput = false;
                $scope.focusTag = true;
            }
            else {
                $scope.focusTag = false;
                $timeout(function () {
                    $scope.focusTag = true;
                }, 500);
                $scope.focusInput = true;
                $timeout(function () {
                    $scope.focusInput = false;
                }, 500);

            }
        }

        function clearScope() {
            $scope.current.tagId = "";
            $scope.current.deviceId = "";
            $scope.focusInTag();

            $rootScope.tagId = "";
            $rootScope.deviceId = "";
            $rootScope.device = {};
            $rootScope.images = [];
        }

        function getTagNumber(taskId) {
            Dataservice.selectItemCount(taskId).then(function (count) {
                $scope.task.itemCount = count;
                $rootScope.task.itemCount = count;
            });
        }

        $scope.goToItemList = function () {
            $rootScope.task = $scope.task;
            if ($scope.current.tagId !== "" || $scope.current.deviceId !== "") {
                $scope.saveTag().then(function () {
                    $state.go("app.itemList");
                });
            }
            else {
                $state.go("app.itemList");
            }
        }

        $scope.goBack = function () {
            $rootScope.images = [];
            $rootScope.device = {};
            $rootScope.itemCount = "";
            $state.go("app.taskList");
        }

        $scope.openMoreInfo = function () {
            $rootScope.task = $scope.task;
            $rootScope.tagId = $scope.current.tagId;
            $rootScope.deviceId = $scope.current.deviceId;
            $state.go("app.moreInfo");
        }

        function deleteTag(tagId) {
            var deferred = $q.defer();
            Dataservice.deleteTag(tagId).then(function () {
                Dataservice.deleteDevices(tagId).then(function () {
                    Dataservice.deleteAttachments(tagId);
                    deferred.resolve();
                });
                Dataservice.deleteFromReplace(tagId);
            });
            return deferred.promise;
        };

        $scope.isDeviceFocus = false;
        $scope.changeFocus = function () {
            $scope.isDeviceFocus = true;
        };
    });