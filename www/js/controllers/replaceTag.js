angular.module('starter.controllers')

    .controller('ReplaceTagCtrl', function ($scope, $rootScope, $state, leafletData, leafletBoundsHelpers, leafletMarkerEvents, $stateParams, Dataservice, $cordovaBarcodeScanner, $cordovaGeolocation, $ionicPopup, $q, $filter, Apiservice, $timeout) {
        $scope.task = {};
        $scope.current = {
            tagId: "",
            deviceId: "",
            comments: ""
        }

        $scope.old = {
            tagId: "",
            deviceId: ""
        }

        if ($stateParams.taskId) {
            $scope.task.name = $stateParams.taskName;
            $scope.task.id = $stateParams.taskId;
            $scope.task.itemCount = $stateParams.itemCount;
            $scope.task.uri = $stateParams.uri;
            $scope.map = $stateParams.map;
            $rootScope.task = $scope.task;
            $rootScope.map = $stateParams.map;
        }
        else {
            $scope.task = $rootScope.task;
            $scope.current.tagId = $rootScope.tagId;
            $scope.current.deviceId = $rootScope.deviceId;
            $scope.map = $rootScope.map;
        }

        $scope.scanOldTagId = function () {
            $cordovaBarcodeScanner.scan().then(function (imageData) {
                var barcode = imageData.text;
                $scope.old.tagId = barcode.substring(barcode.lastIndexOf("/") + 1);
                console.log("Barcode Format -> " + imageData.format);
                console.log("Cancelled -> " + imageData.cancelled);
            }, function (error) {
                console.log("An error happened -> " + error);
            });
        }


        $scope.scanOldDeviceId = function () {
            $cordovaBarcodeScanner.scan().then(function (imageData) {
                var barcode = imageData.text;
                $scope.old.deviceId = barcode;
                console.log("Barcode Format -> " + imageData.format);
                console.log("Cancelled -> " + imageData.cancelled);
            }, function (error) {
                console.log("An error happened -> " + error);
            });
        }

        $scope.scanNewTagId = function () {
            $cordovaBarcodeScanner.scan().then(function (imageData) {
                //alert(imageData.text);
                var barcode = imageData.text;
                $scope.current.tagId = barcode.substring(barcode.lastIndexOf("/") + 1);
                console.log("Barcode Format -> " + imageData.format);
                console.log("Cancelled -> " + imageData.cancelled);
            }, function (error) {
                console.log("An error happened -> " + error);
            });
        }


        $scope.scanNewDeviceId = function () {
            $cordovaBarcodeScanner.scan().then(function (imageData) {
                var barcode = imageData.text;
                $scope.current.deviceId = barcode;
                console.log("Barcode Format -> " + imageData.format);
                console.log("Cancelled -> " + imageData.cancelled);
            }, function (error) {
                console.log("An error happened -> " + error);
            });
        }

        function validateTagId(barcode) {
            var crc = crc32(barcode.substring(5));
            var checkWith = barcode.substring(3, 5);
            if (checkWith.toLowerCase() === crc.substring(crc.length - 2).toLowerCase()) {
                return true;
            }
            else {
                return false;
            }
        }

        function makeCRCTable() {
            var c;
            var crcTable = [];
            for (var n = 0; n < 256; n++) {
                c = n;
                for (var k = 0; k < 8; k++) {
                    c = ((c & 1) ? (0xEDB88320 ^ (c >>> 1)) : (c >>> 1));
                }
                crcTable[n] = c;
            }
            return crcTable;
        }

        function crc32(str) {
            var crcTable = window.crcTable || (window.crcTable = makeCRCTable());
            var crc = 0 ^ (-1);

            for (var i = 0; i < str.length; i++) {
                crc = (crc >>> 8) ^ crcTable[(crc ^ str.charCodeAt(i)) & 0xFF];
            }
            var dec = (crc ^ (-1)) >>> 0;
            var hexString = dec.toString(16)
            return hexString;
        };

        $scope.saveTag = function () {
            var deferred = $q.defer();
            if ($scope.current.tagId.indexOf("/") !== -1) {
                $scope.current.tagId = $scope.current.tagId.substring($scope.current.tagId.lastIndexOf("/") + 1);
            }
            if ($scope.old.tagId.indexOf("/") !== -1) {
                $scope.old.tagId = $scope.old.tagId.substring($scope.old.tagId.lastIndexOf("/") + 1);
            }
            //var isOldValid = validateTagId($scope.old.tagId);
            var isNewValid = validateTagId($scope.current.tagId);
            if (/*!isOldValid ||*/ !isNewValid) {
                alert($filter('translate')('INVALIDTAG'));
                //if (!isNewValid)
                $scope.current.tagId = "";
                // if (!isOldValid)
                //     $scope.old.tagId = "";
                return;
            }
            //var isOldHex = $scope.old.deviceId.match("[0-9A-Fa-f]{8}");
            var isCurrentHex = $scope.current.deviceId.match("[0-9A-Fa-f]{8}");
            console.log("Device details: " + $scope.current.deviceId);
            console.log(isCurrentHex);
            if (/*!isOldHex ||*/ !isCurrentHex) {
                alert($filter('translate')('INVALIDDEVICE'));
                // if (!isOldHex)
                //     $scope.old.deviceId = "";
                //if (!isCurrentHex)

                $scope.current.deviceId = "";
                return;
            }
            console.log($scope.current.comments);
            if (!$scope.current.comments) {
                alert($filter('translate')('MISSINGCOMMENT'));
                return;
            }
            if ($scope.current.tagId !== "" && $scope.current.deviceId !== "" && $scope.old.tagId !== "" && $scope.old.deviceId !== "") {
                var currentTags = [];
                Dataservice.getTag($scope.current.tagId).then(function (res) {
                    if (res) {
                        for (var i = 0; i < res.rows.length; i++) {
                            currentTags.push(res.rows.item(i).tagId);
                        }
                    } else {
                        console.log("No results found");
                    }
                    console.log("Current tags: " + currentTags);
                    if (currentTags && (currentTags.indexOf($scope.current.tagId) !== -1 || currentTags.indexOf($scope.old.tagId) !== -1)) {
                        $ionicPopup.show({
                            template: $filter('translate')('TAG_EXISTS'),
                            title: $filter('translate')('DUPLICATETAG'),
                            scope: $scope,
                            buttons: [{
                                text: $filter('translate')('KEEPOLD'),
                                type: 'button-large button-bar',
                                onTap: function (e) {
                                    clearScope();
                                    deferred.resolve();
                                }
                            },
                            {
                                text: $filter('translate')('USENEW'),
                                type: 'button-large',
                                onTap: function (e) {
                                    deleteTags([$scope.current.tagId, $scope.old.tagId]).then(function () {
                                        Dataservice.insertTag($scope.task.id, $scope.current.tagId).then(function () {
                                            Dataservice.insertTag($scope.task.id, $scope.old.tagId).then(function () {
                                                Dataservice.insertIntoReplace($scope.task.id, $scope.old, $scope.current, $scope.current.comments);
                                                getTagNumber($scope.task.id);
                                                clearScope();
                                                deferred.resolve();
                                            });
                                        });
                                    });
                                }
                            }]
                        });
                    }
                    else {
                        Dataservice.insertTag($scope.task.id, $scope.current.tagId).then(function () {
                            Dataservice.insertTag($scope.task.id, $scope.old.tagId).then(function () {
                                Dataservice.insertIntoReplace($scope.task.id, $scope.old, $scope.current, $scope.current.comments);
                                getTagNumber($scope.task.id);
                                deferred.resolve();
                                clearScope();
                            });
                        });
                    }
                });
            }
            else {
                alert($filter('translate')('MISSINGTAGDEVICE'));
            }
            return deferred.promise;
        }

        function clearScope() {
            $scope.current.tagId = "";
            $scope.current.deviceId = "";
            $scope.old.tagId = "";
            $scope.old.deviceId = "";
            $scope.current.comments = ""
            $scope.focusOldTagIn();
            $rootScope.tagId = "";
        }

        $scope.focusOldDeviceIn = function () {
            if (!$scope.focusOldDevice) {
                $scope.focusOldDevice = true;
            }
            else {
                $scope.focusOldDevice = false;
                $timeout(function () {
                    $scope.focusOldDevice = true;
                });
            }
            $scope.focusNewTag = false;
            $scope.focusNewDevice = false;
            $scope.focusComment = false;
            $scope.focusOldTag = false;
        }

        $scope.focusNewTagIn = function () {
            $scope.focusOldDevice = false;
            if (!$scope.focusNewTag) {
                $scope.focusNewTag = true;
            }
            else {
                $scope.focusNewTag = false;
                $timeout(function () {
                    $scope.focusNewTag = true;
                });
            }
            $scope.focusNewDevice = false;
            $scope.focusComment = false;
            $scope.focusOldTag = false;
        }

        $scope.focusNewDeviceIn = function () {
            $scope.focusOldDevice = false;
            $scope.focusNewTag = false;
            if (!$scope.focusNewDevice) {
                $scope.focusNewDevice = true;
            }
            else {
                $scope.focusNewDevice = false;
                $timeout(function () {
                    $scope.focusNewDevice = true;
                });
            }
            $scope.focusComment = false;
            $scope.focusOldTag = false;
        }

        $scope.focusCommentIn = function () {
            $scope.focusOldDevice = false;
            $scope.focusNewTag = false;
            $scope.focusNewDevice = false;
            if (!$scope.focusComment) {
                $scope.focusComment = true;
            }
            else {
                $scope.focusComment = false;
                $timeout(function () {
                    $scope.focusComment = true;
                });
            }
            $scope.focusOldTag = false;
        }

        $scope.focusOldTagIn = function () {
            $scope.focusOldDevice = false;
            $scope.focusNewTag = false;
            $scope.focusNewDevice = false;
            $scope.focusComment = false;
            $timeout(function () {
                if (!$scope.focusOldTag) {
                    $scope.focusOldTag = true;
                }
                else {
                    $scope.focusOldTag = false;
                    $timeout(function () {
                        $scope.focusOldTag = true;
                    });
                }
            }, 500);
        }

        function getTagNumber(taskId) {
            Dataservice.selectItemCountForReplace(taskId).then(function (count) {
                $scope.task.itemCount = count;
                $rootScope.task.itemCount = count;
            });
        }

        $scope.goToItemList = function () {
            $rootScope.task = $scope.task;
            if ($scope.current.tagId !== "" || $scope.current.deviceId !== "" || $scope.old.tagId !== "" || $scope.old.deviceId !== "") {
                $scope.saveTag().then(function () {
                    $state.go("app.itemList");
                });
            }
            else {
                $state.go("app.itemList");
            }
        }

        $scope.goBack = function () {
            $rootScope.itemCount = "";
            $state.go("app.taskList");
        }

        function deleteTags(tagIds) {
            var deferred = $q.defer();
            for (var i = 0; i < tagIds.length; i++) {
                (function (i) {
                    Dataservice.deleteTag(tagIds[i]).then(function () {
                        Dataservice.deleteDevices(tagIds[i]).then(function () {
                            Dataservice.deleteAttachments(tagIds[i]);
                            deferred.resolve();
                        });
                        Dataservice.deleteFromReplace(tagIds[i]);
                    });
                })(i);
            }
            return deferred.promise;
        };

        $scope.copyTagId = function () {
            $scope.current.tagId = $scope.old.tagId;
        };

        $scope.copyDeviceId = function () {
            $scope.current.deviceId = $scope.old.deviceId;
        };
    });