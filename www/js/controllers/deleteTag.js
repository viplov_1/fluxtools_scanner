angular.module('starter.controllers')

    .controller('DeleteTagCtrl', function ($scope, $rootScope, $state, leafletData, leafletBoundsHelpers, leafletMarkerEvents, $stateParams, Dataservice, $cordovaBarcodeScanner, $cordovaGeolocation, $ionicPopup, $q, $filter, Apiservice) {
        $scope.task = {};
        $scope.current = {
            tagId: "",
            comments: ""
        }

        if ($stateParams.taskId) {
            $scope.task.name = $stateParams.taskName;
            $scope.task.id = $stateParams.taskId;
            $scope.task.itemCount = $stateParams.itemCount;
            $scope.map = $stateParams.map;
            $scope.task.uri = $stateParams.uri;
            $rootScope.task = $scope.task;
            $rootScope.map = $stateParams.map;
        }
        else {
            $scope.task = $rootScope.task;
            $scope.current.tagId = $rootScope.tagId;
            $scope.map = $rootScope.map;
        }

        $scope.scanTagId = function () {
            $cordovaBarcodeScanner.scan().then(function (imageData) {
                var barcode = imageData.text;
                $scope.current.tagId = barcode.substring(barcode.lastIndexOf("/") + 1);
                console.log("Barcode Format -> " + imageData.format);
                console.log("Cancelled -> " + imageData.cancelled);
            }, function (error) {
                console.log("An error happened -> " + error);
            });
        }

        function validateTagId(barcode) {
            var crc = crc32(barcode.substring(5));
            var checkWith = barcode.substring(3, 5);
            if (checkWith.toLowerCase() === crc.substring(crc.length - 2).toLowerCase()) {
                return true;
            }
            else {
                return false;
            }
        }

        function makeCRCTable() {
            var c;
            var crcTable = [];
            for (var n = 0; n < 256; n++) {
                c = n;
                for (var k = 0; k < 8; k++) {
                    c = ((c & 1) ? (0xEDB88320 ^ (c >>> 1)) : (c >>> 1));
                }
                crcTable[n] = c;
            }
            return crcTable;
        }

        function crc32(str) {
            var crcTable = window.crcTable || (window.crcTable = makeCRCTable());
            var crc = 0 ^ (-1);

            for (var i = 0; i < str.length; i++) {
                crc = (crc >>> 8) ^ crcTable[(crc ^ str.charCodeAt(i)) & 0xFF];
            }
            var dec = (crc ^ (-1)) >>> 0;
            var hexString = dec.toString(16)
            return hexString;
        };

        $scope.saveTag = function () {
            var deferred = $q.defer();
            if ($scope.current.tagId.indexOf("/") !== -1) {
                $scope.current.tagId = $scope.current.tagId.substring($scope.current.tagId.lastIndexOf("/") + 1);
            }
            var isValid = validateTagId($scope.current.tagId);
            if (!isValid) {
                alert($filter('translate')('INVALIDTAG'));
                $scope.current.tagId = "";
                return;
            }

            if (!$scope.current.comments) {
                alert($filter('translate')('MISSINGCOMMENT'));
                return;
            }

            if ($scope.current.tagId !== "") {
                var currentTags = [];
                Dataservice.getTag($scope.current.tagId).then(function (res) {
                    if (res) {
                        for (var i = 0; i < res.rows.length; i++) {
                            console.log(res.rows);
                            currentTags.push(res.rows.item(i).tagId);
                        }
                    } else {
                        console.log("No results found");
                    }
                    console.log("Current tags: " + currentTags);
                    if (currentTags && currentTags.indexOf($scope.current.tagId) !== -1) {
                        $ionicPopup.show({
                            template: $filter('translate')('TAG_EXISTS'),
                            title: $filter('translate')('DUPLICATETAG'),
                            subTitle: '',
                            scope: $scope,
                            buttons: [{
                                text: $filter('translate')('KEEPOLD'),
                                type: 'button-large button-bar',
                                onTap: function (e) {
                                    clearScope();
                                    deferred.resolve();
                                }
                            },
                            {
                                text: $filter('translate')('USENEW'),
                                type: 'button-large',
                                onTap: function (e) {
                                    deleteTag($scope.current.tagId).then(function () {
                                        Dataservice.insertTag($scope.task.id, $scope.current.tagId).then(function () {
                                            getTagNumber($scope.task.id);
                                            Dataservice.insertDevice($scope.current.tagId, "", { description: $scope.current.comments }, "", 0).then(function () {
                                                deferred.resolve();
                                            });
                                        });
                                    });
                                }
                            }]
                        });
                    }
                    else {
                        Dataservice.insertTag($scope.task.id, $scope.current.tagId).then(function () {
                            Dataservice.insertDevice($scope.current.tagId, "", { description: $scope.current.comments }, "", 0).then(function () {
                                deferred.resolve();
                                getTagNumber($scope.task.id);
                                clearScope();
                            });
                        });
                    }
                });
            }
            else {
                alert($filter('translate')('MISSINGTAGDEVICE'));
            }
            return deferred.promise;
        }

        function clearScope() {
            $scope.current.tagId = "";
            $rootScope.tagId = "";
        }

        function getTagNumber(taskId) {
            Dataservice.selectItemCount(taskId).then(function (count) {
                $scope.task.itemCount = count;
                $rootScope.task.itemCount = count;
            });
        }

        $scope.goToItemList = function () {
            $rootScope.task = $scope.task;
            if ($scope.current.tagId !== "") {
                $scope.saveTag().then(function () {
                    $state.go("app.itemList");
                });
            }
            else {
                $state.go("app.itemList");
            }
        }

        $scope.goBack = function () {
            $rootScope.itemCount = "";
            $state.go("app.taskList");
        }

        $scope.openMoreInfo = function () {
            $rootScope.task = $scope.task;
            $rootScope.tagId = $scope.current.tagId;
            $state.go("app.moreInfo");
        }

        function deleteTag(tagId) {
            var deferred = $q.defer();
            Dataservice.deleteTag(tagId).then(function () {
                Dataservice.deleteDevices(tagId).then(function () {
                    Dataservice.deleteAttachments(tagId);
                    deferred.resolve();
                });
                Dataservice.deleteFromReplace(tagId);
            });
            return deferred.promise;
        };
    });