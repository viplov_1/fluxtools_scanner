angular.module('starter.controllers')

    .controller('TaskListCtrl', function ($scope, $rootScope, $state, $cordovaBarcodeScanner, $ionicPopup, $q, $ionicLoading, $filter, $ionicPopover, Apiservice, Dataservice, $timeout) {
        var currentTasks = [];
        var refObj = new Object();
        $scope.data = {};
        $scope.tasks = [];
        $ionicPopover.fromTemplateUrl('templates/popover.html', {
            scope: $scope,
        }).then(function (popover) {
            $scope.popover = popover;
        });

        refreshTasks();
        function refreshTasks() {
            Dataservice.selectFromTask().then(function (res) {
                $scope.tasks = [];
                console.log(res);
                if (res && res.rows && res.rows.length > 0) {
                    addItems(res.rows);
                }
            });
        }

        function addItems(rows) {
            for (var i = 0; i < rows.length; i++) {
                var task = {
                    id: rows.item(i).id,
                    subId: rows.item(i).id.substring(rows.item(i).id.length - 12),
                    name: rows.item(i).name,
                    description: rows.item(i).description,
                    syncStatus: rows.item(i).syncStatus,
                    itemCount: parseInt(rows.item(i).itemCount),
                    uri: rows.item(i).uri,
                    map: rows.item(i).map,
                    type: rows.item(i).type
                };
                if (task.syncStatus && task.itemCount > 0) {
                    task.hasSyncFailed = true;
                }
                console.log(task);
                $scope.tasks.push(task);
            }
            for (var i = 0; i < $scope.tasks.length; i++) {
                (function (i) {
                    var thumbnail = "";
                    if ($scope.tasks[i].map.toLowerCase() === "google") {
                        $scope.tasks[i].thumbnail = "img/google.png";
                    }
                    else if ($scope.tasks[i].map.toLowerCase() === "baidu") {
                        $scope.tasks[i].thumbnail = "img/baidu.png";
                    }
                    else {
                        console.log("Getting plan for " + i);
                        console.log($scope.tasks[i]);
                        Dataservice.getPlan($scope.tasks[i].map).then(function (data) {
                            $scope.tasks[i].thumbnail = data;
                        }, function (err) {
                            Apiservice.getFloorPlanImage(err).then(function (data) {
                                $scope.tasks[i].thumbnail = data;
                                Dataservice.insertPlan(err, data);
                            });
                        });

                    }
                    if ($scope.tasks[i].type === "replace") {
                        Dataservice.selectItemCountForReplace($scope.tasks[i].id).then(function (count) {
                            console.log(count);
                            if (count) {
                                $scope.tasks[i].itemCount = count;
                            }
                            else {
                                $scope.tasks[i].itemCount = 0;
                                $scope.tasks[i].hasSyncFailed = false;
                                Dataservice.updateSyncStatus($scope.tasks[i].id, "Complete");
                                Dataservice.updateItemCount($scope.tasks[i].id, 0);
                            }
                        });
                    }
                    else {
                        Dataservice.selectItemCount($scope.tasks[i].id).then(function (res) {
                            $scope.tasks[i].itemCount = res;
                            if ($scope.tasks[i].itemCount === 0) {
                                $scope.tasks[i].hasSyncFailed = false;
                                Dataservice.updateSyncStatus($scope.tasks[i].id, "Complete");
                                Dataservice.updateItemCount($scope.tasks[i].id, 0);
                            }
                        });
                    }
                    if ($scope.tasks[i].hasSyncFailed) {
                        console.log($scope.tasks[i].name);
                        console.log($scope.tasks[i].itemCount);

                        if ($scope.tasks[i].type === "replace") {
                            Dataservice.getReasonForReplace($scope.tasks[i].id).then(function (response) {
                                $scope.tasks[i].syncReason = response;
                            });
                        }
                        else {
                            var firstSelected = false;
                            Dataservice.getTags($scope.tasks[i].id).then(function (tags) {
                                for (var j = 0; j < tags.length; j++) {
                                    (function (j) {
                                        var tag = tags[j];
                                        console.log(tag);
                                        Dataservice.getDevices(tag).then(function (rows) {
                                            if (rows && rows.item(0) && rows.item(0).syncReason && !firstSelected) {
                                                $scope.tasks[i].syncReason = rows.item(0).syncReason;
                                                firstSelected = true;
                                            }
                                        });
                                    })(j);
                                }
                            });
                        }
                        if ($scope.tasks[i].itemCount === 0) {
                            $scope.tasks[i].hasSyncFailed = false;
                            Dataservice.updateSyncStatus($scope.tasks[i].id, "Complete");
                            Dataservice.updateItemCount($scope.tasks[i].id, 0);
                        }
                    }
                })(i);
            }
        }

        $scope.scanBarcode = function () {
            $cordovaBarcodeScanner.scan().then(function (imageData) {
                console.log(imageData);
                var barcode = JSON.parse(imageData.text);
                console.log(barcode.Map);
                Dataservice.insertIntoTask(barcode.TaskID, barcode.Name, barcode.Descr, "", barcode.Uri, barcode.Type, barcode.Map, "0").then(function () {
                    refreshTasks();
                });
                console.log("Barcode Format -> " + imageData.format);
                console.log("Cancelled -> " + imageData.cancelled);
            }, function (error) {
                console.log("An error happened -> " + error);
            });
            $scope.popover.hide();
        };

        $scope.clearTasks = function () {
            var itemsToBeSynced = false;
            for (var i = 0; i < $scope.tasks.length; i++) {
                if ($scope.tasks[i].itemCount !== 0) {
                    console.log($scope.tasks[i]);
                    itemsToBeSynced = true;
                    break;
                }
            }
            if (itemsToBeSynced) {
                $ionicPopup.show({
                    template: $filter('translate')('CLEAR_ALL_PROMPT'),
                    title: $filter('translate')('CLEAR_ALL'),
                    subTitle: '',
                    scope: $scope,
                    buttons: [{
                        text: $filter('translate')('YES'),
                        type: 'button-large button-bar button-dark',
                        onTap: function (e) {
                            clearAll();
                        }
                    },
                    {
                        text: 'No',
                        type: 'button-large'
                    }]
                });
            }
            else {
                clearAll();
            }
            $scope.popover.hide();
        };

        function clearAll() {
            Dataservice.clearTasks().then(function () {
                deleteTags("", "").then(function () {
                    refreshTasks();
                });
            });
        }

        $scope.showProgress = function () {
            for (var i = 0; i < $scope.tasks.length; i++) {
                $scope.tasks[i].isSyncing = true;
            }
        };
        $scope.hideProgress = function (task) {
            for (var i = 0; i < $scope.tasks.length; i++) {
                $scope.tasks[i].isSyncing = false;
            }
        };

        $scope.syncWrap = function () {
            var loginRequired = false;
            for (var i = 0; i < $scope.tasks.length; i++) {
                var task = $scope.tasks[i];
                if (task.type === "replace" || task.type === "delete") {
                    if (task.itemCount > 0) {
                        loginRequired = true;
                        break;
                    }
                }
            }
            Dataservice.getLoginDetails().then(function (rows) {
                $scope.user.email = rows.item(0).email;
                $scope.user.host = rows.item(0).provisioning;
            },
                function () {
                    $scope.user.email = "";
                    $scope.user.host = "prov1.8maps.com";
                }
            );
            if (loginRequired) {
                $ionicPopup.show({
                    templateUrl: "templates/login.html",
                    title: $filter('translate')('LOGIN'),
                    scope: $scope,
                    buttons: [{
                        text: $filter('translate')('LOGIN'),
                        type: 'button-large button-bar button-dark',
                        onTap: function (e) {
                            Dataservice.deleteLoginDetails();
                            Dataservice.insertLoginDetails($scope.user.email, $scope.user.host);
                            Apiservice.login($scope.user).then(function (response) {
                                $scope.user.password = "";
                                $scope.showProgress();
                                $scope.syncNow().then(function () {
                                    Apiservice.logout();
                                    $scope.hideProgress();
                                    refreshTasks();
                                }
                                );
                            }, function (err) {
                                Apiservice.logout();
                                $scope.syncWrap();
                                $scope.user.password = "";
                            });
                        }
                    },
                    {
                        text: $filter('translate')('CANCEL'),
                        type: 'button-large button-bar button-dark'
                    }
                    ]
                });
            }
            else {
                $scope.showProgress();
                $scope.syncNow().then(function () {
                    $scope.hideProgress();
                    refreshTasks();
                });
            }
        }

        $scope.syncNow = function () {
            var deferred = $q.defer();
            var host = "prov1.8maps.com";
            if ($scope.user.host) {
                host = $scope.user.host;
            }
            var count = 0;
            for (var i = 0; i < $scope.tasks.length; i++) {
                (function (i) {
                    console.log($scope.tasks[i]);
                    if ($scope.tasks[i].type === "replace") {
                        Dataservice.getTagsFromReplace($scope.tasks[i].id).then(function (replaceTags) {
                            var syncReplaceTags = [];
                            for (var i = 0; i < replaceTags.length; i++) {
                                var replaceObj = {
                                    OLDTagID: replaceTags.item(i).oldTag,
                                    OLDDeviceID: replaceTags.item(i).oldDevice,
                                    TagID: replaceTags.item(i).newTag,
                                    DeviceID: replaceTags.item(i).newDevice,
                                    Comment: replaceTags.item(i).comment
                                }
                                syncReplaceTags.push(replaceObj);
                            }
                            syncReplace($scope.tasks[i].id, syncReplaceTags, host).then(function () {
                                count++;
                                if (count === $scope.tasks.length) {
                                    deferred.resolve();
                                }
                            },
                                function () {
                                    $scope.tasks[i].hasSyncFailed = true;
                                    if (count === $scope.tasks.length) {
                                        deferred.resolve();
                                    }
                                }
                            );
                        });
                    }
                    else {
                        Dataservice.getTags($scope.tasks[i].id).then(function (tags) {
                            console.log(tags);
                            syncTags($scope.tasks[i], tags, host).then(function () {
                                console.log($scope.tasks[i]);
                                console.log(i);
                                count++;
                                console.log(count);
                                if (count === $scope.tasks.length) {
                                    deferred.resolve();
                                }
                            },
                                function (err) {
                                    console.log($scope.tasks[i]);
                                    count++;
                                    console.log("sync failed for " + $scope.tasks[i].id);
                                    $scope.tasks[i].hasSyncFailed = true;
                                    if (count === $scope.tasks.length) {
                                        deferred.resolve();
                                    }
                                }
                            );
                        },
                            function (err) {
                                count++;
                                console.log(err);
                                console.log(count);
                                if (count === $scope.tasks.length) {
                                    deferred.resolve();
                                }
                            }
                        );
                    }
                })(i);
            }

            return deferred.promise;
        };

        function syncReplace(task, currentTags, host) {
            var deferred = $q.defer();
            var synced = 0;
            console.log(currentTags);
            if (currentTags.length === 0) {
                deferred.resolve();
            }
            for (var i = 0; i < currentTags.length; i++) {
                (function (i) {
                    Apiservice.syncReplace(currentTags[i], host).then(function (data) {
                        deleteTags(currentTags[i]).then(function () {
                            synced++;
                            if (synced === currentTags.length) {
                                deferred.resolve();
                            }
                        })
                    },
                        function (err) {
                            synced++;
                            if (synced === currentTags.length) {
                                deferred.resolve();
                            }
                            Dataservice.updateSyncStatus(task, "").then(function () {
                                Dataservice.updateSyncReasonForReplace(task, currentTags[i].TagID, "", "Connection failed");
                            });
                        });
                })(i);
            }
            return deferred.promise;
        }

        function syncTags(task, currentTags, host) {
            var deferred = $q.defer();
            var synced = 0;
            console.log(currentTags);
            if (currentTags.length === 0) {
                deferred.resolve();
            }
            for (var i = 0; i < currentTags.length; i++) {
                (function (i) {
                    var tagId = currentTags[i];
                    Dataservice.getDevices(tagId).then(function (rows) {
                        if (rows.length > 0) {
                            switch (task.type) {
                                case "delete":
                                    var desc = rows.item(0).deviceDescription;
                                    Apiservice.deleteTag(tagId, desc, host).then(function () {
                                        synced++;
                                        if (synced === currentTags.length) {
                                            deferred.resolve();
                                        }
                                    },
                                        function (err) {
                                            synced++;
                                            if (synced === currentTags.length) {
                                                deferred.resolve();
                                            }
                                            updateSyncStatus(task.id, tagId, "", "Connection failed");
                                        }
                                    );
                                    break;
                                case "GPS":
                                    var tag = {
                                        ID: tagId,
                                        GPS: rows.item(0).coords
                                    }
                                    Apiservice.updateGPS(tag, host).then(function () {
                                        synced++;
                                        if (synced === currentTags.length) {
                                            deferred.resolve();
                                        }
                                    },
                                        function (err) {
                                            synced++;
                                            if (synced === currentTags.length) {
                                                deferred.resolve();
                                            }
                                            updateSyncStatus(task.id, tagId, "", "Connection failed");
                                        }
                                    );
                                    break;
                                case "move":
                                    var tag = {
                                        taskid: task.id,
                                        Tag: tagId
                                    }
                                    Apiservice.moveTag(tag, host).then(function () {
                                        synced++;
                                        if (synced === currentTags.length) {
                                            deferred.resolve();
                                        }
                                    },
                                        function (err) {
                                            synced++;
                                            if (synced === currentTags.length) {
                                                deferred.resolve();
                                            }
                                            updateSyncStatus(task.id, tagId, "", "Connection failed");
                                        }
                                    );
                                    break;
                                case "assign":
                                    var tag = {
                                        TaskID: task.id,
                                        TagID: tagId,
                                        DeviceID: rows.item(0).deviceId
                                    };
                                    syncWithServer(tag, task.uri, "POST", "").then(function () {
                                        synced++;
                                        console.log(synced);
                                        if (synced === currentTags.length) {
                                            deferred.resolve();
                                        }
                                    });
                                    break;
                                case "lite":
                                case "install":
                                default:
                                    var tag = {
                                        TaskID: task.id,
                                        TagID: tagId,
                                        DeviceID: rows.item(0).deviceId,
                                        GPS: rows.item(0).coords,
                                        Name: rows.item(0).deviceName,
                                        Description: rows.item(0).deviceDescription,
                                        Attachment: []
                                    };
                                    Dataservice.getAttachments(tag.TagID).then(function (attachments) {
                                        tag.Attachment = attachments;
                                        syncWithServer(tag, task.uri, "POST", "").then(function () {
                                            synced++;
                                            console.log(synced);
                                            if (synced === currentTags.length) {
                                                deferred.resolve();
                                            }
                                        });
                                    });
                                    break;
                            }
                        }
                    });
                })(i);
            }
            return deferred.promise;
        }

        function syncWithServer(tag, uri, method, value) {
            var deferred = $q.defer();
            Apiservice.syncWithServer(tag, uri, method, value).then(function (response) {
                console.log(tag);
                updateSyncStatus(tag.TaskID, tag.TagID, true, "");
                deleteTags(tag.TaskID, tag.TagID, false).then(function () {
                    deferred.resolve();
                });
            }, function (data, status, headers, config) {
                var failureReasonObject = data;
                console.log(failureReasonObject);
                if (failureReasonObject && failureReasonObject.length >= 1) {
                    var failureReason = failureReasonObject[0].type;
                    var failureValue = failureReasonObject[0].value;
                    var append = "";
                    var prepend = "";
                    if (failureValue === "loctag_info.TagID") {
                        append = " tag " + tag.TagID;
                        prepend = "Tag";
                    }
                    else {
                        append = " device " + tag.DeviceID;
                        prepend = "Device";
                    }
                    if (failureReason === "ERROR_MESSAGE.IS_DULPICATE") {
                        if ($rootScope.futureSelection === "") {
                            $ionicPopup.show({
                                template: prepend + '{{\'TAG_SYNCED\' | translate}}<br/><ion-checkbox ng-model="data.isChecked">{{\'APPLYALL\' | translate}}</ion-checkbox>',
                                title: $filter('translate')('DUPLICATETAG') + append,
                                subTitle: '',
                                scope: $scope,
                                buttons: [{
                                    text: $filter('translate')('OVERWRITE'),
                                    type: 'button button-bar',
                                    onTap: function (e) {
                                        if ($scope.data.isChecked) {
                                            $rootScope.futureSelection = "ow";
                                        }
                                        syncWithServer(tag, uri, "PUT", failureValue).then(function () {
                                            deferred.resolve();
                                        });
                                    }
                                },
                                {
                                    text: $filter('translate')('REMOVE'),
                                    type: 'button',
                                    onTap: function (e) {
                                        if ($scope.data.isChecked) {
                                            $rootScope.futureSelection = "r";
                                        }
                                        deleteTags(tag.TaskID, tag.TagID, false).then(function () {
                                            deferred.resolve();
                                        });
                                        console.log("syncWithServer: Resolved");
                                    }
                                }]
                            });
                        }
                        else {
                            if ($rootScope.futureSelection === "ow") {
                                syncWithServer(tag, uri, "PUT", failureValue).then(function () {
                                    deferred.resolve();
                                });
                            }
                            else {
                                deleteTags(tag.TaskID, TagID, false).then(function () {
                                    deferred.resolve();
                                });
                            }
                        }
                    }
                    else {
                        var reason = "";
                        if (data && data.length >= 1) {
                            reason = getReasonMessage(data[0].type, data[0].value);
                        }
                        if (!reason) {
                            reason = "Not found";
                        }
                        console.log(tag);
                        updateSyncStatus(tag.TaskID, tag.TagID, false, reason).then(function () {
                            deferred.resolve();
                        });
                        deferred.resolve();
                        console.log("syncWithServer: Resolved");
                    }
                }
                else {
                    var reason = "";
                    if (data && data.length >= 1) {
                        reason = getReasonMessage(data[0].type, data[0].value);
                    }
                    if (!reason) {
                        reason = "Not found";
                    }
                    console.log(reason);
                    updateSyncStatus(tag.TaskID, tag.TagID, reason).then(function () {
                        deferred.resolve();
                    });
                    console.log("syncWithServer: Resolved");
                }
            }
            );
            return deferred.promise;
        }

        function getReasonMessage(reasonType, reasonValue) {
            switch (reasonType) {
                case "ERROR_MESSAGE.NOT_EXIST":
                    if (reasonValue === "loctag_info.TagID") {
                        return "Update tag";
                    }
                    else {
                        return "Create and update tag";
                    }
                    break;
                case "ERROR_MESSAGE.IS_DULPICATE":
                    if (reasonValue === "loctag_info.TagID") {
                        return "Create tag";
                    }
                    else {
                        return "Update tag";
                    }
                    break;
                case "ERROR_MESSAGE.PERMISSION_DENIED":
                    return "Update tag and update device";
            }
        }

        function deleteTags(taskId, tagId, isClear) {
            var deferred = $q.defer();
            Dataservice.deleteTags(taskId, tagId).then(function () {
                Dataservice.deleteDevices(tagId).then(function () {
                    if (isClear) {
                        $scope.tasks = [];
                    }
                    Dataservice.deleteAttachments(tagId);
                    deferred.resolve();
                });
                Dataservice.deleteFromReplace(tagId);
            });
            return deferred.promise;
        }

        function updateSyncStatus(taskId, tagId, status, syncReason) {
            var deferred = $q.defer();
            Dataservice.updateSyncStatus(taskId, status).then(function (res) {
                if (!status) {
                    Dataservice.updateSyncReason(tagId, status, syncReason);
                }
                deferred.resolve();
            });
            return deferred.promise;
        }

        $scope.user = {
            email: "",
            password: "",
            host: ""
        }

        $scope.clearErrors = function () {
            var total = 0;
            var complete = 0;
            for (var i = 0; i < $scope.tasks.length; i++) {
                (function (i) {
                    var task = $scope.tasks[i];
                    if (task.syncStatus !== "") {
                        total++;
                        Dataservice.updateSyncStatus(task.id, "Complete").then(function () {
                            complete++;
                            if (complete === total) {
                                refreshTasks();
                            }
                            Dataservice.getTags(task.id).then(function (tags) {
                                for (var i = 0; i < tags.length; i++) {
                                    (function (i) {
                                        Dataservice.updateSyncReason(tags[i], "Complete", "");
                                        Dataservice.updateSyncReasonForReplace(task.id, tags[i], "Complete", "");
                                    })(i);
                                }
                            });
                        });
                    }
                })(i);
            }
            $scope.popover.hide();
        }

        $scope.clearAllData = function () {
            $scope.tasks = [];
            Dataservice.dropAllTables();
            $scope.popover.hide();
        }

        $scope.downloadTasks = function () {
            Dataservice.getLoginDetails().then(function (rows) {
                $scope.user.email = rows.item(0).email;
                $scope.user.host = rows.item(0).provisioning;
            },
                function () {
                    $scope.user.email = "";
                    $scope.user.host = "prov1.8maps.com";
                }
            );

            $ionicPopup.show({
                templateUrl: "templates/login.html",
                title: $filter('translate')('LOGIN'),
                scope: $scope,
                buttons: [{
                    text: $filter('translate')('LOGIN'),
                    type: 'button-large button-bar button-dark',
                    onTap: function (e) {
                        Dataservice.deleteLoginDetails();
                        Dataservice.insertLoginDetails($scope.user.email, $scope.user.host);
                        Apiservice.login($scope.user).then(function (response) {
                            $scope.user.password = "";
                            Apiservice.getAllTasks().then(function (response) {
                                for (var i = 0; i < $scope.tasks.length; i++) {
                                    var task = $scope.tasks[i];
                                    refObj[task.id] = i;
                                }
                                var refArray = Object.keys(refObj);
                                var duplicateArray = [];
                                for (var i = 0; i < response.length; i++) {
                                    var item = response[i];
                                    console.log(item);
                                    var task = {
                                        id: item.TaskID,
                                        subId: item.TaskID.substring(item.TaskID.length - 12),
                                        name: item.Name,
                                        description: item.Description,
                                        syncStatus: "",
                                        itemCount: 0,
                                        uri: $scope.user.host + "/api/scan",
                                        map: item.Location.Map,
                                        type: item.Type
                                    };
                                    if (!task.type) {
                                        task.type = ""
                                    }
                                    if (refArray.indexOf(task.TaskID) !== -1) {
                                        duplicateArray.push(refObj[task.TaskID]);
                                        Dataservice.deleteTask(task.TaskID);
                                    }
                                    $scope.tasks.push(task);
                                    Dataservice.insertIntoTask(task.id, task.name, task.description, task.syncStatus, task.uri, task.type, task.map, task.itemCount);
                                    if (duplicateArray.length > 0) {
                                        for (var i = 0; i < duplicateArray.length; i++) {
                                            var index = $scope.indexOf(duplicateArray[i]);
                                            if (index !== -1) {
                                                $scope.tasks.splice(index, 1);
                                            }
                                        }
                                    }
                                }
                                for (var i = 0; i < $scope.tasks.length; i++) {
                                    var thumbnail = "";
                                    if ($scope.tasks[i].map.toLowerCase() === "google") {
                                        $scope.tasks[i].thumbnail = "img/google.png";
                                    }
                                    else if ($scope.tasks[i].map.toLowerCase() === "baidu") {
                                        $scope.tasks[i].thumbnail = "img/baidu.png";
                                    }
                                    else {
                                        (function (i) {
                                            console.log("Getting plan for " + i);
                                            console.log($scope.tasks[i]);
                                            Dataservice.getPlan($scope.tasks[i].map).then(function (data) {
                                                $scope.tasks[i].thumbnail = data;
                                            }, function (err) {
                                                Apiservice.getFloorPlanImage(err).then(function (data) {
                                                    $scope.tasks[i].thumbnail = data;
                                                    Dataservice.insertPlan(err, data);
                                                });
                                            });
                                        })(i);
                                    }
                                }
                                Apiservice.logout();
                                console.log($scope.tasks);
                            }, function (error) {
                                $scope.downloadTasks();
                            });
                        },
                            function () {
                                console.log("Login failed");
                                $scope.user.password = "";
                                $scope.downloadTasks();
                            }
                        );
                        $scope.popover.hide();
                    }
                },
                {
                    text: $filter('translate')('CANCEL'),
                    type: 'button-large button-bar button-dark',
                    onTap: function (e) {
                        $scope.popover.hide();
                    }
                }
                ]
            });
            //$scope.popover.hide();
        }

        $scope.goToTypeScreen = function (task) {
            var taskObj = {
                taskId: task.id,
                taskName: task.name,
                itemCount: task.itemCount,
                map: task.map,
                uri: task.uri
            };
            $rootScope.type = task.type;
            console.log(task.type);

            switch (task.type) {
                case "GPS":
                    $state.go("app.updategps", taskObj);
                    break;
                case "delete":
                    $state.go("app.deletetag", taskObj);
                    break;
                case "replace":
                    $state.go("app.replacetag", taskObj);
                    break;
                case "lite":
                    $state.go("app.lite", taskObj);
                    break;
                case "assign":
                    $state.go("app.assign", taskObj);
                    break;
                case "move":
                    $state.go("app.move", taskObj);
                    break;
                default:
                    $state.go("app.mapScan", taskObj);
            }
        }

    });
