'use strict';

angular.module('starter.controllers', ['ngCordova'])

    .controller('AppCtrl', function ($scope, $rootScope, $state, $cordovaSQLite) {
        //--------------------

        var permissions = cordova.plugins.permissions;
        permissions.hasPermission(permissions.CAMERA, checkPermissionCallback, null);

        function checkPermissionCallback(status) {
            if (!status.hasPermission) {
                var errorCallback = function () {
                    console.warn('Camera permission is not turned on');
                    ionic.Platform.exitApp();
                }

                permissions.requestPermission(
                    permissions.CAMERA,
                    function (status) {
                        if (!status.hasPermission) errorCallback();
                    },
                    errorCallback);
            }
        }
        $rootScope.futureSelection = "";
        console.log("Initializing future selection");
    })

    .config([
        '$compileProvider',
        function ($compileProvider) {
            $compileProvider.imgSrcSanitizationWhitelist(/^\s*(https?|file|blob|cdvfile):|data:image\//);
        }
    ]);


