// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
var db = null;
angular.module('starter',
  ['starter.controllers',
    'ionic',
    'pascalprecht.translate',
    "tmh.dynamicLocale",
    'ngCordova',
    'nemLogging',
    'ui-leaflet'
  ])

  .constant("availableLanguages", ["en-us", "zh-tw", "zh-cn", "hi-in"])
  .constant("defaultLanguage", "en-us")

  .run(function ($ionicPlatform, tmhDynamicLocale, $translate, $cordovaGlobalization, availableLanguages, $rootScope, defaultLanguage, $locale, $cordovaSQLite, $q) {

    $ionicPlatform.ready(function () {
      setLanguage();
      if (window.cordova && window.cordova.plugins.Keyboard) {
        // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
        // for form inputs)
        cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);

        // Don't remove this line unless you know what you are doing. It stops the viewport
        // from snapping when text inputs are focused. Ionic handles this internally for
        // a much nicer keyboard experience.
        cordova.plugins.Keyboard.disableScroll(true);
      }
      if (window.StatusBar) {
        StatusBar.styleDefault();
      }
      //---------------
      db = $cordovaSQLite.openDB({ name: 'new.db', location: 'default' }, 1);
      $cordovaSQLite.execute(db, "CREATE TABLE IF NOT EXISTS tasks (id text primary key, name text,description text, syncStatus text, uri text, type text, map text, itemCount text)");
      $cordovaSQLite.execute(db, "CREATE TABLE IF NOT EXISTS tags (taskId text, tagId text)");
      $cordovaSQLite.execute(db, "CREATE TABLE IF NOT EXISTS devices (tagId text primary key, deviceId text, deviceName text, deviceDescription text, syncStatus text, syncReason text, coords text)");
      $cordovaSQLite.execute(db, "CREATE TABLE IF NOT EXISTS attachments (tagId text, fileName text, attachment text, comment text)");
      $cordovaSQLite.execute(db, "CREATE TABLE IF NOT EXISTS floorPlan (fpId text, plan text)");
      $cordovaSQLite.execute(db, "CREATE TABLE IF NOT EXISTS replaceTable (taskId text, oldTag text, oldDevice text, newTag text, newDevice text, comment text, syncReason text)");
      $cordovaSQLite.execute(db, "CREATE TABLE IF NOT EXISTS loginCache (email text, provisioning text)");
      //xxxxxxxxx
    });

    $ionicPlatform.registerBackButtonAction(function () {
      navigator.app.exitApp();
    }, 100);

    function setLanguage() {
      if (typeof navigator.globalization !== 'undefined') {
        $cordovaGlobalization.getPreferredLanguage().then(function (result) {
          var language = getSuitableLanguage(result.value);

          applyLanguage(language);
          $translate.use(language);
        });
      }
      else {
        applyLanguage('en-us');
      }
    }

    function getSuitableLanguage(language) {
      for (var index = 0; index < availableLanguages.length; index++) {
        if (availableLanguages[index].toLowerCase() === language.toLowerCase()) {
          return availableLanguages[index];
        }
      }
      return defaultLanguage;
    }

    function applyLanguage(language) {
      tmhDynamicLocale.set(language.toLowerCase());
    }
  })

  .config(function (tmhDynamicLocaleProvider, $translateProvider, defaultLanguage) {
    tmhDynamicLocaleProvider.localeLocationPattern('locales/angular-locale_{{locale}}.js');
    $translateProvider.useStaticFilesLoader({
      'prefix': 'i18n/',
      'suffix': '.json'
    });
    $translateProvider.preferredLanguage(defaultLanguage);
  })

  .config(function ($stateProvider, $urlRouterProvider, $ionicConfigProvider) {

    // Turn off caching for demo simplicity's sake
    $ionicConfigProvider.views.maxCache(0);
    $ionicConfigProvider.tabs.position('bottom');
    /*
    // Turn off back button text
    $ionicConfigProvider.backButton.previousTitleText(false);
    */

    $stateProvider.state('app', {
      url: '/app',
      abstract: true,
      templateUrl: 'templates/menu.html',
      controller: 'AppCtrl',
      // -------------
      resolve: {
        cordova: function ($q) {
          var deferred = $q.defer();
          ionic.Platform.ready(function () {
            deferred.resolve();
          });
          return deferred.promise;
        }
      }
      // ----xxxxxx------
    })
      .state('app.taskList', {
        url: '/tasklist',
        views: {
          'menuContent': {
            templateUrl: 'templates/taskList.html',
            controller: 'TaskListCtrl'
          }
        }
      })

      .state('app.mapScan', {
        url: '/mapscan?taskId&taskName&itemCount&map&uri',
        views: {
          'menuContent': {
            templateUrl: 'templates/mapscan.html',
            controller: 'MapScanCtrl'
          }
        }
      })

      .state('app.moreInfo', {
        url: '/moreinfo',
        views: {
          'menuContent': {
            templateUrl: 'templates/moreinfo.html',
            controller: 'MoreInfoCtrl'
          }
        }
      })

      .state('app.itemList', {
        url: '/itemlist',
        views: {
          'menuContent': {
            templateUrl: 'templates/itemlist.html',
            controller: 'ItemListCtrl'
          }
        }
      })

      .state('app.updategps', {
        url: '/updategps?taskId&taskName&itemCount&map&uri',
        views: {
          'menuContent': {
            templateUrl: 'templates/updategps.html',
            controller: 'UpdateGPSCtrl'
          }
        }
      })

      .state('app.deletetag', {
        url: '/deletetag?taskId&taskName&itemCount&map&uri',
        views: {
          'menuContent': {
            templateUrl: 'templates/deletetag.html',
            controller: 'DeleteTagCtrl'
          }
        }
      })

      .state('app.replacetag', {
        url: '/replacetag?taskId&taskName&itemCount&map&uri',
        views: {
          'menuContent': {
            templateUrl: 'templates/replacetag.html',
            controller: 'ReplaceTagCtrl'
          }
        }
      })

      .state('app.lite', {
        url: '/lite?taskId&taskName&itemCount&map&uri',
        views: {
          'menuContent': {
            templateUrl: 'templates/lite.html',
            controller: 'LiteCtrl'
          }
        }
      })

      .state('app.assign', {
        url: '/assign?taskId&taskName&itemCount&map&uri',
        views: {
          'menuContent': {
            templateUrl: 'templates/assign.html',
            controller: 'AssignCtrl'
          }
        }
      })

      .state('app.move', {
        url: '/move?taskId&taskName&itemCount&map&uri',
        views: {
          'menuContent': {
            templateUrl: 'templates/move.html',
            controller: 'MoveCtrl'
          }
        }
      })

    // if none of the above states are matched, use this as the fallback
    $urlRouterProvider.otherwise('/app/tasklist');
  })